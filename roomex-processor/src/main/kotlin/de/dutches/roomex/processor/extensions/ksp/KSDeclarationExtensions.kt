package de.dutches.roomex.processor.extensions.ksp

import com.google.devtools.ksp.symbol.KSDeclaration
import de.dutches.roomex.processor.extensions.resolveName
import kotlin.reflect.KClass

/**
 * Check if the given annotation is present on the parent declaration of the given one
 *
 * @param annotation Annotation to check for
 * @return True if the given declaration is present
 */
internal fun KSDeclaration.hasParentAnnotation(annotation: KClass<out Annotation>) : Boolean
{
    if (this.parentDeclaration != null)
        return this.parentDeclaration!!.isAnnotationPresent(annotation)
    return false
}

/**
 * Check if the given annotation is present at the given declaration
 *
 * Custom implementation is necessary since default KSP isAnnotationPresent is experimental
 * @param annotation Annotation to check for
 * @return True if the given declaration is present
 * @see com.google.devtools.ksp.isAnnotationPresent
 */
internal fun KSDeclaration.isAnnotationPresent(annotation : KClass<out Annotation>) : Boolean
{
    return this.annotations.any { x -> x.resolveName() == annotation.resolveName() }
}

/**
 * Check if any of the given annotations is present at the given declaration
 *
 * Custom implementation is necessary since default KSP isAnnotationPresent is experimental
 * @param annotations Annotations to check for
 * @return True if any of the given declarations is present
 * @see com.google.devtools.ksp.isAnnotationPresent
 */
internal fun KSDeclaration.isAnnotationPresent(annotations : Array<KClass<out Annotation>>) : Boolean
{
    val localAnnotations = this.annotations.map { x -> x.resolveName() }
    return annotations.any { x -> localAnnotations.contains(x.resolveName() )}
}

/**
 * Retrieve the argument value of an annotation.
 *
 * @param annotation Annotation with the argument
 * @param argument The argument of which the value needs to be fetched
 * @return The found value. String and char values are returned escaped.
 */
internal fun KSDeclaration.fetchAnnotationArgValue(annotation : KClass<out Annotation>, argument : String) : Any?
    = this.annotations.firstOrNull { it.resolveName() == annotation.resolveName() }?.fetchArgValue(argument)

internal fun KSDeclaration.fetchAnnotationArgValue(annotationQualifiedNames : List<String>, argument : String) : Any? {

    return this.annotations.firstOrNull { it.resolveName() in annotationQualifiedNames }?.fetchArgValue(argument)
}

/**
 * Resolve the qualified, or the simple name of the given declaration.
 *
 * @return Name as string
 */
internal fun KSDeclaration.resolveName(simpleName: Boolean = false) : String {
    return  if (simpleName)
        this.simpleName.asString()
    else
        this.qualifiedName?.asString() ?: (this.packageName.asString() + this.simpleName.asString())
}