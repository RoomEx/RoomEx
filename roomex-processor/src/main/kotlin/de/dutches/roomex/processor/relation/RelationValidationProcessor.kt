package de.dutches.roomex.processor.relation

import androidx.room.Entity
import androidx.room.Ignore
import com.google.devtools.ksp.closestClassDeclaration
import com.google.devtools.ksp.getAllSuperTypes
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.KSType
import de.dutches.roomex.processor.AbstractProcessor
import de.dutches.roomex.processor.CollectorProcessor
import de.dutches.roomex.processor.Constants
import de.dutches.roomex.processor.extensions.ksp.*
import de.dutches.roomex.processor.extensions.ksp.hasParentAnnotation
import de.dutches.roomex.processor.extensions.ksp.isAnnotationPresent
import de.dutches.roomex.processor.extensions.resolveName
import de.dutches.roomex.processor.helper.relations.RelationUtils
import de.dutches.roomex.storage.model.RoomExEntity
import de.dutches.roomex.storage.model.relation.*
import kotlin.reflect.KClass

abstract class RelationValidationProcessor(
    codeGenerator: CodeGenerator,
    logger: KSPLogger,
    options: Map<String, String>
) : AbstractProcessor(codeGenerator, logger, options) {

    protected abstract val allowedMappedAnnotations : Array<KClass<out Annotation>>

    override fun getVisitor() = Visitor()

    protected open inner class RelationVisitor : Visitor() {

        override fun visitPropertyDeclaration(property: KSPropertyDeclaration, data: Unit) {
            super.visitPropertyDeclaration(property, data)
            val resolvedType = property.type.resolve()

            // Check that @Ignore annotation exists
            if (!property.isAnnotationPresent(Ignore::class))
                logger.error("Annotation ${ManyToManyEx::class.simpleName} must be combined with the Room ${Ignore::class.simpleName} annotation.",property)

            // Check that the containing class has the @Entity annotation
            if (!property.hasParentAnnotation(Entity::class))
                logger.error("Annotation ${annotationClass.simpleName} can only be used with entities decorated with the Room ${Entity::class.simpleName} annotation.",property)

            // Check that the target field is nullable
            if (!resolvedType.isMarkedNullable)
                logger.error("The target of the ${annotationClass.simpleName} annotation must be nullable",property)

            // Check mutability
            if (!property.isMutable)
                logger.error("The target of the ${annotationClass.simpleName} annotation must be mutable",property)

            val propType: KSType?
            val childEntityType: KSType?
            try {
                val (tmpPropType, tmpChildEntityType) = RelationUtils.fetchEntityTypeForProperty(
                    property,
                    CollectorProcessor.allEntitiesWithRelations.keys,
                    CollectorProcessor.entityInterfaceMapping
                )
                propType = tmpPropType
                childEntityType = tmpChildEntityType
            } catch (ex: Throwable) {
                logger.error(ex.message.toString(), property)
                return
            }

            if (childEntityType == null) {
                logger.error("Unable to resolve property ${property.simpleName.asString()} to an entity.", property)
                return
            }

            // Add only to cache of property type has been resolved, and it's not a collection
            if (propType != null && propType.arguments.any())
                CollectorProcessor.entityInterfaceMapping[propType] = childEntityType

            // Get value of "mappedWith"
            val mappedWith = property.fetchAnnotationArgValue(annotationClass.kotlin,
                Constants.Annotations.Relations.Arguments.MAPPED_WITH) as String

            // Should mappedWith, contains a value, check if that Field exists
            if (mappedWith.isNotEmpty()) {
                val targetField = childEntityType
                    .declaration
                    .closestClassDeclaration()!!
                    .getAllProperties()
                    .firstOrNull { prop -> prop.simpleName.asString() == mappedWith }

                // check if given field exists
                if (targetField == null) {
                    logger.error(
                        "${Constants.Annotations.Relations.Arguments.MAPPED_WITH} argument must reference an existing relation property in the target entity",
                        property
                    )
                    return
                }

                // Check that target field != source field
                if (targetField.qualifiedName?.asString() == property.qualifiedName?.asString()) {
                    logger.error("Target field of ${Constants.Annotations.Relations.Arguments.MAPPED_WITH} can not be the source field", property)
                    return
                }

                // Check if field contains a relation annotation
                if (!targetField.isAnnotationPresent(arrayOf(ManyToManyEx::class, OneToManyEx::class, ManyToOneEx::class, OneToOneEx::class))) {
                    logger.error("Target field of ${Constants.Annotations.Relations.Arguments.MAPPED_WITH} must be annotated with a relation annotation", property)
                    return
                }

                // Check if field annotation is allowed
                if (!targetField.annotations.any { a -> allowedMappedAnnotations.map { allowed -> allowed.qualifiedName.toString() }.contains(a.resolveName()) }  ) {
                    logger.error("Target field annotation mismatch. Allowed annotations: ${allowedMappedAnnotations.joinToString { allowedAnn -> allowedAnn.simpleName.toString() }}", property)
                    return
                }


                val targetFieldType: KSType?
                try {
                    val (_, tmpChildEntityType) = RelationUtils.fetchEntityTypeForProperty(
                        targetField,
                        CollectorProcessor.allEntitiesWithRelations.keys,
                        CollectorProcessor.entityInterfaceMapping
                    )
                    targetFieldType = tmpChildEntityType
                } catch (ex: Throwable) {
                    logger.error(ex.message.toString(), property)
                    return
                }

                // Check if given field targets the current entity
                if (targetFieldType == null) {
                    logger.error("Unable to resolve property ${targetField.simpleName.asString()} to an entity.\n" +
                         "${Constants.Annotations.Relations.Arguments.MAPPED_WITH} argument must reference an existing relation property in the target entity", property)
                    return
                }

                // Add only to cache of property type has been resolved, and it's not a collection
                if (propType != null && propType.arguments.any())
                    CollectorProcessor.entityInterfaceMapping[propType] = targetFieldType

                // Check if target type is of the current type
                if (targetFieldType.declaration.qualifiedName?.asString() != property.closestClassDeclaration()?.qualifiedName?.asString()) {
                    logger.error("${Constants.Annotations.Relations.Arguments.MAPPED_WITH} type mismatch!", property)
                    logger.error(targetFieldType.declaration.qualifiedName?.asString() + " --- " + property.closestClassDeclaration()?.qualifiedName?.asString())
                    return
                }

                // Get the mappedWith value of the target field, since that one must be empty
                val mappedWithTargetField = targetField.fetchAnnotationArgValue(annotationClass.kotlin, Constants.Annotations.Relations.Arguments.MAPPED_WITH) as String

                // Check if given field contains a relation annotation with a mappedWith value
                if (mappedWithTargetField.isNotEmpty()) {
                    logger.error(
                        "${Constants.Annotations.Relations.Arguments.MAPPED_WITH} argument can not be used while reference a relation property which already uses it",
                        property
                    )
                    return
                }
            }
        }
    }

    protected fun checkTargetChildEntity(property: KSPropertyDeclaration, resolvedType: KSType) : Boolean {
        return ((resolvedType.declaration.closestClassDeclaration()!!.getAllSuperTypes().any { s -> s.declaration.qualifiedName!!.asString() == RoomExEntity::class.resolveName()})
            || ((property.annotations.firstOrNull { a -> a.resolveName() == annotationClass.typeName }
                ?.arguments?.firstOrNull { it.name?.asString() == Constants.Annotations.Relations.Arguments.ENTITY_CLASS }
                ?.value as KSType).declaration.qualifiedName.toString() != RoomExEntity::class.qualifiedName.toString()))
    }
}