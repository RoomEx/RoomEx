# RoomEx

Extension for `Android Room` to simplify creating, resolving and working with database relations.   
RoomEx allows you to define relations between entities:

```Kotlin
@Entity
class Address(
    var streetName: String,
    var city: String,
    var houseNumber: String

): RoomExEntity() { }
```

```Kotlin
@Entity
class User(
    var userName: String,
    var email: String

): RoomExEntity() { 

    @Ignore 
    @OneToOneEx(cascade = Cascade.All) // Here, a 1:1 relation will be created, with create, read, update and delete cascade
    var address: Address? = null
}
```

## Installing

### With repository
RoomEx has been released on the Maven Central Repository, therefore you can add RoomEx as follows:

```Kotlin
implementation("de.dutches.roomex:roomex-android:1.7.21-0.9.0") // 1.7.21 = kotlin version, 0.9.0 = roomex version
implementation("de.dutches.roomex:roomex-common:1.7.21-0.9.0")
ksp("de.dutches.roomex:roomex-processor:1.7.21-0.9.0")
```

Also add the KSP plugin to your plugins:
```Kotlin
id("com.google.devtools.ksp") version "1.7.21-1.0.8"
```
## Built With

  - [Room](https://developer.android.com/reference/androidx/room/package-summary) - Database Object Mapping library 
  - [KSP](https://github.com/google/ksp) - Kotlin Symbol Processing API
  - [KotlinPoet](https://github.com/square/kotlinpoet) - API for generating code files

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code
of conduct, and the process for submitting pull requests to us.

## Versioning

We use [Semantic Versioning](http://semver.org/) for versioning. 
RoomEx has been built using KSP. KSP releases are tightly coupled to specific Kotlin versions.    
Therefore, make sure the version matches your Kotlin version.
RoomEx uses the following naming pattern for releases:

> RoomEx-KotlinVersion-RoomExVersion

For the versions available, see the [tags on this
repository](https://codeberg.org/RoomEx/RoomEx/tags).

## Authors

  - **Marcel van der Heide** - *Idea and initial source code* -
    [FlyingDutchman](https://codeberg.org/FlyingDutchman)

## License

This project is licensed under the [MIT license](LICENSE.md) - 
see the [LICENSE.md](LICENSE.md) file for details
