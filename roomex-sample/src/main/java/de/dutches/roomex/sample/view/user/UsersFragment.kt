package de.dutches.roomex.sample.view.user

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import de.dutches.roomex.sample.BaseFragment
import de.dutches.roomex.sample.R
import de.dutches.roomex.sample.databinding.FragmentUsersBinding
import de.dutches.roomex.sample.model.User

@AndroidEntryPoint
class UserFragment : BaseFragment(R.layout.fragment_users), UsersAdapter.OnItemClickListener,
    AdapterView.OnItemClickListener {

    private val viewModel: UserViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentUsersBinding.bind(view)
        val userAdapter = UsersAdapter()

        binding.apply {
            cachesRecyclerView.apply {
                adapter = userAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }
        }

        viewModel.users.observe(viewLifecycleOwner) {
            userAdapter.submitList(it)
        }
    }

    override fun onItemClick(user: User) { } // no action needed
    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) { } // no action needed
}