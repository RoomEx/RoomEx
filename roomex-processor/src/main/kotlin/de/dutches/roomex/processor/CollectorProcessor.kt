package de.dutches.roomex.processor

import androidx.room.*
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.symbol.*
import de.dutches.roomex.processor.helper.DaoUtils

/**
 * This annotation processor does not handle any annotation. Instead, its purpose is to collect and resolve
 * elements over multiple round of processing, which are then being used by the other processors.
 *
 * @param codeGenerator
 * @param logger
 * @param options
 */
class CollectorProcessor(
    codeGenerator: CodeGenerator,
    logger: KSPLogger,
    options: Map<String, String>
) : AbstractProcessor(codeGenerator, logger, options) {

    // Dummy annotation which we won't be using
    override val annotationClass = Query::class.java

    companion object ResolvedSymbols {
        // Instance is kept alive between runs, therefore we are able to store data between runs
        var allEntitiesWithRelations = mutableMapOf<KSType, MutableMap<String, Boolean>>()
        var allDaoWithEntity = mutableMapOf<KSDeclaration, KSType>()
        var entityInterfaceMapping = mutableMapOf<KSType, KSType>()
    }

    override fun runVisitor(validSymbols: List<KSDeclaration>): List<KSDeclaration> {
        // Fetch all dao's and entities from this run
        allEntitiesWithRelations += fetchAllEntities()
        allDaoWithEntity += fetchAllDaosWithEntity()

        return super.runVisitor(validSymbols)
    }

    private fun fetchAllDaosWithEntity() : Map<KSDeclaration, KSType> {
        val map = mutableMapOf<KSDeclaration, KSType>()

        for (dao in resolver.getSymbolsWithAnnotation(Dao::class.java.name).filterIsInstance<KSClassDeclaration>().toList()) {
            map[dao] = DaoUtils.fetchEntityForDao(dao)
        }

        return map
    }

    private fun fetchAllEntities() : Map<KSType, MutableMap<String, Boolean>> {
        val ret = mutableMapOf<KSType, MutableMap<String, Boolean>>()
        resolver
            .getSymbolsWithAnnotation(Entity::class.java.name)
            .filterIsInstance<KSClassDeclaration>()
            .forEach { ret[it.asStarProjectedType()] = mutableMapOf() }

        return  ret
    }

}