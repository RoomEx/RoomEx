@file:Suppress("unused")
package de.dutches.roomex.storage.crud

import androidx.room.Insert
import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Interface to add public methods to your Dao to insert
 * new entities in the database.
 *
 * @param T Entity type
 */
interface ICreate<T> where T : RoomExEntity {
    /**
     * Insert a new entity in the database.
     *
     * @param entity The entity to store in the database.
     * @return New id of the entity.
     */
    @Insert
    suspend fun insert(entity: T): Long

    /**
     * Insert a new entity and its children in the database.
     *
     * @param entity The entity to store in the database.
     * @param recursively If children of the children.
     * @return New id of the parent entity.
     */
    suspend fun insertWithChildren(entity: T, recursively: Boolean = false): Long

    /**
     * Insert a new entity and its children in the database.
     * Note: If a child already has an id,
     *
     * @param entity The entity to store in the database.
     * @param recursively Should children be fetched recursively.
     * @param fields The field name of any child fields to process.
     * @return New id of the parent entity.
     */
    suspend fun insertWithChildren(entity: T, recursively: Boolean = false, vararg fields: String): Long
}