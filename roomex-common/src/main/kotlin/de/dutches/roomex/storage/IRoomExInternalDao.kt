package de.dutches.roomex.storage

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update
import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Interface for internal use by RoomEx
 * Defines CRUD-methods for RoomExEntities
 */
interface IRoomExInternalDao<T> where T : RoomExEntity {

    /**
     * Inserts the given entity in the database.
     *
     * @param entity The entity to insert.
     * @return The id of the inserted entity.
     */
    @Insert
    suspend fun rinsert(entity : T) : Long

    /**
     * Updates the given entity to the database.
     *
     * @param entity The entity do update.
     * @return Effected rows count.
     */
    @Update
    suspend fun rupdate(entity: T) : Int

    /**
     * Deletes the given entity from the database.
     *
     * @param entity The entity to delete.
     * @return Effected rows count.
     */
    @Delete
    suspend fun rdelete(entity: T) : Int

    /**
     * Reads the entity with the given id from the database.
     *
     * @param id The Id of the entity to read.
     * @return Entity instance.
     */
    suspend fun rread(id: Long) : T?

    /**
     * Get all entities from the database.
     *
     * @return All entities in the database.
     */
    suspend fun rreadAll() : MutableList<T>

    /**
     * Checks if a en entity with the given id exists.
     *
     * @param id The id of the entity to check.
     * @return True if entity exists.
     */
    suspend fun rexists(id: Long): Boolean

    /** Usefully information about the entity for a Dao */
    val daoData: DaoData

    /**
     * Read a single relation property value from an entity instance.
     *
     * @param entity The entity to read the value from
     * @param name The name of the property field
     * @return Either the set entity, or null if no entity has been set
     */
    fun readPropertySingle(entity: T, name: String): RoomExEntity?

    /**
     * Read a list relation property value from an entity instance.
     *
     * @param entity The entity to read the value from
     * @param name The name of the property field
     * @return Either the set list, or null if the list has been set
     */
    fun readPropertyList(entity: T, name: String): List<RoomExEntity>?

    /**
     * Write to a single relation property value from an entity instance.
     *
     * @param entity The entity to write the value to
     * @param name The name of the property field
     * @param value The value for the property
     */
    fun writePropertySingle(entity: T, name: String, value: RoomExEntity?)

    /**
     * Write to a list relation property value from an entity instance.
     *
     * @param entity The entity to write the value to
     * @param name The name of the property field
     * @param value The value for the property
     */
    fun writePropertyList(entity: T, name: String, value: MutableList<RoomExEntity>?)
}