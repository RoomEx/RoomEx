package de.dutches.roomex.storage.crud

import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Interface to add public methods to your Dao to add or remove
 * joins in the database.
 *
 * @param T Entity type
 */
interface IJoins<T> where T : RoomExEntity {

    /**
     * Joins the given entities with one another.
     * Should the child not have a valid id, it will be inserted in the database and
     * the new id will be set on the object
     *
     * @param entity Parent entity
     * @param child Child entity
     * @param fieldName The field name in the parent model
     */
    suspend fun addJoin(entity: T, child: RoomExEntity, fieldName: String)

    /**
     * Joins the given entities with one another.
     * Should any of the children not have a valid id, it will be inserted in the database and
     * the new id will be set on the object
     *
     * @param entity Parent entity
     * @param children Child entity
     * @param fieldName The field name in the parent model
     */
    suspend fun addJoins(entity: T, children: List<RoomExEntity>, fieldName: String)

    /**
     * Removes the join between the given entities
     *
     * @param entity Parent entity
     * @param child Child entity
     * @param fieldName The field name in the parent model
     */
    suspend fun removeJoin(entity: T, child: RoomExEntity, fieldName: String)

    /**
     * Removes the join between the given entities
     *
     * @param entity Parent entity
     * @param children Child entity
     * @param fieldName The field name in the parent model
     */
    suspend fun removeJoins(entity: T, children: List<RoomExEntity>, fieldName: String)

    /**
     * Removes any existing join, if the corresponding field is null, and only null.
     * Only the join will be removed, not the corresponding child entity.
     *
     * @param entity The entity for which the relations should be updated.
     * @param fields Any field which needs to be checked for removal. When not passing any field name, all
     * relation fields are checked.
     */
    suspend fun removeJoinsIfNull(entity: T, vararg fields : String)
}