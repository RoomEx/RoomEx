import org.jetbrains.kotlin.gradle.plugin.mpp.pm20.util.archivesName
import java.util.*
import java.net.URI

plugins {
    id("kotlin-android")
    id("kotlin-kapt")
    id("com.android.library")
    id("org.jetbrains.dokka") version "1.7.20"
    id("idea")
    `maven-publish`
    signing
}

group = getSetting("roomExGroupName")
version = "${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}"

android {
    compileSdk = 33
    namespace = getSetting("roomExGroupName")

    @Suppress("UnstableApiUsage")
    defaultConfig {
        minSdk = 19
        targetSdk = 33
        version = "${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        archivesName.set("${project.name}-${project.version}")
    }

    @Suppress("UnstableApiUsage")
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
    }
}

repositories {
    mavenCentral()
    mavenLocal()
    google()
}


idea {
    module {
        isDownloadSources = true
    }
}

dependencies {
    //api(project(":roomex-common"))
    api("de.dutches.roomex:roomex-common:${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}")

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${getSetting("kotlinVersion")}")
    implementation("androidx.room:room-common:${getSetting("roomVersion")}")
    implementation("androidx.room:room-runtime:${getSetting("roomVersion")}")
}


val isReleaseVersion = !getSetting("roomExVersion").endsWith("SNAPSHOT")
val artifact = "$buildDir/outputs/aar/${project.name}-${project.version}-release.aar"

val dokkaHtml by tasks.getting(org.jetbrains.dokka.gradle.DokkaTask::class)

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    from(kotlin.sourceSets.main.get().kotlin)
}

val javadocJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Javadoc JAR"
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    archiveClassifier.set("javadoc")
    from(dokkaHtml)
}

artifacts {
    archives(sourcesJar)
    archives(javadocJar)
}

publishing {


    repositories {
        maven {
            val releaseRepo = URI("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
            val snapshotRepo = URI("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            name = "OSSRH"
            url = if (isReleaseVersion) releaseRepo else snapshotRepo

            credentials {
                username = getSetting("ossrh_user")
                password = getSetting("ossrh_pass")
            }
        }
    }

    publications {

        create<MavenPublication>("maven") {
            // Add sources and documentation
            artifact(javadocJar)
            artifact(sourcesJar)
            artifact(artifact)

            // Build the POM
            pom {
                groupId = getSetting("roomExGroupName")
                version = "${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}"

                name.set(project.name)
                inceptionYear.set("2022")
                url.set(getSetting("pom_url"))
                description.set("Android package for RoomEx, a relationship manager for Android Room.")
                packaging = "aar"

                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://opensource.org/licenses/MIT")
                    }
                }

                developers {
                    developer {
                        id.set(getSetting("pom_dev_id"))
                        name.set(getSetting("pom_dev_name"))
                        email.set(getSetting("pom_dev_mail"))
                        url.set(getSetting("pom_dev_url"))
                    }
                }

                issueManagement {
                    system.set(getSetting("pom_issue_system"))
                    url.set(getSetting("pom_issue_url"))
                }

                scm {
                    connection.set(getSetting("pom_scm_con"))
                    developerConnection.set(getSetting("pom_scm_dev_con"))
                    url.set(getSetting("pom_scm_dev_con"))
                }

                // Because we're not using "from(components["release"])", we need to add dependencies manually
                withXml {
                    val dependenciesNode = asNode().appendNode("dependencies")
                    configurations.getByName("implementation") {
                        dependencies.forEach {
                            val dependencyNode = dependenciesNode.appendNode("dependency")
                            dependencyNode.appendNode("groupId", it.group)
                            dependencyNode.appendNode("artifactId", it.name)
                            dependencyNode.appendNode("version", it.version)
                        }
                    }
                }
            }
        }
    }
}

signing {
    useGpgCmd()
    sign(artifact)
    sign(publishing.publications)
}

tasks.withType<Sign> {
    onlyIf { isReleaseVersion }
}

// We are not using "from(components...)" therefore, trigger a build before publishing
tasks.filter { task -> task.name.toLowerCase().contains("maven") || task.name.toLowerCase().contains("publish") }.forEach {
    it.dependsOn("build")
}

fun getSetting(key: String) = project.getSetting(key)
fun Project.getSetting(key: String): String {

    // Check if local properties
    val localProperty = rootProject.file("local.properties")
    if (localProperty.isFile) {
        val props = Properties().apply { load(localProperty.inputStream())}
        if (props.containsKey(key))
            return props[key].toString()
    }

    // Not in local, check gradle properties
    if (!project.properties.containsKey(key))
        throw Exception("Key $key not found in gradle.properties")
    return project.properties[key].toString()
}