package de.dutches.roomex.sample.dao

//import androidx.test.filters.SmallTest
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.dutches.roomex.sample.model.Address
import de.dutches.roomex.sample.model.Contact
//import androidx.test.filters.SmallTest
import de.dutches.roomex.sample.model.User
import de.dutches.roomex.sample.storage.local.dao.AddressDao
import de.dutches.roomex.sample.storage.local.dao.ContactDao
import de.dutches.roomex.sample.storage.local.dao.UserDao
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class ContactDaoTest : BaseDaoTest() {

    private lateinit var userDao: UserDao
    private lateinit var addressDao: AddressDao
    private lateinit var contactDao: ContactDao

    @Before
    fun setup() {
        userDao = db.fetchDaoForEntity(User::class) as UserDao
        addressDao = db.fetchDaoForEntity(Address::class) as AddressDao
        contactDao = db.fetchDaoForEntity(Contact::class) as ContactDao
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun insertNewContact() = runTest {
        // Given
        val contact = Contact("54654", "test@test.de")

        // When
        val id = contactDao.insert(contact)

        // Then
        assertNotEquals(id, 0L)
    }

    @Test
    fun getNewContactAfterInsert() = runTest {

        // Given
        val id = contactDao.insert(Contact("54654", "test@test.de"))

        // When
        val contact = contactDao.getById(id)

        // Then
        assertNotNull(contact)
        assertNotEquals(contact!!.id, 0L)
    }

    @Test
    fun getMappedAddress() = runTest {

        // Given - Create new address with contact
        val address = Address("54654", "asdad")
        address.contact = Contact("54654", UUID.randomUUID().toString())
        address.id = addressDao.insertWithChildren(address)
        addressDao.loadChildren(address)


        // When
        val contact = contactDao.getByIdWithChildren(address.contact!!.id)

        // Then
        assertNotNull(address.contact)
        assertNotNull(contact)
        assertNotNull(contact!!.address)
        assertNotEquals(contact.address!!.id, 0L)
        assertEquals(contact.address!!.id, address.id)
    }
}
