package de.dutches.roomex.processor.helper.relations

import androidx.room.Entity
import com.google.devtools.ksp.getAllSuperTypes
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import com.google.devtools.ksp.symbol.KSType
import de.dutches.roomex.processor.Constants
import de.dutches.roomex.processor.extensions.kotlinpoet.resolveTypeName
import de.dutches.roomex.processor.extensions.ksp.fetchAnnotationArgValue
import de.dutches.roomex.processor.extensions.ksp.fetchArgValue
import de.dutches.roomex.processor.extensions.ksp.isAnnotationPresent
import de.dutches.roomex.processor.extensions.ksp.resolveName
import de.dutches.roomex.storage.model.RoomExEntity

internal object RelationUtils {

    //region FETCH ENTITY TYPE
    /**
     * Resolves the type of the property and the type of the entity for that property.
     * In most cases these types are identical -> type of property is an entity class.
     * Should the type be an interface, will try to resolve the interface to an entity.
     *
     * Property type will not be resolved when entity argument of the relation annotation
     * has been set.
     *
     * @param property
     * @param allEntities
     * @param cachedInterfaceMapping
     * @return
     */
    internal fun fetchEntityTypeForProperty(property: KSPropertyDeclaration,
                                            allEntities: Collection<KSType>,
                                            cachedInterfaceMapping: MutableMap<KSType, KSType>)
    : Pair<KSType?, KSType?> {

        // First, check if type has been given by annotation
        var entityType = fetchValueEntityClassArg(property)
        if (entityType != null)
            return Pair(null, entityType)

        // Resolve property
        var resolvedPropertyType = property.type.resolve()
        if (resolvedPropertyType.arguments.count() == 1)
            resolvedPropertyType = resolvedPropertyType.arguments.first().type!!.resolve()

        // Secondly, check if already cached
        entityType = cachedInterfaceMapping.getOrDefault(resolvedPropertyType, null)
        if (entityType != null)
            return Pair(resolvedPropertyType, entityType)

        // Thirdly, check if property is an entity
        if (resolvedPropertyType.declaration.isAnnotationPresent(Entity::class))
            return Pair(resolvedPropertyType, resolvedPropertyType)

        // Property is either a base class, or an interface, need to resolve to entity
        // Fourthly, go over all entities and check inheritance
        entityType = fetchEntityClassByInheritanceCheck(resolvedPropertyType, allEntities)
        if (entityType != null) {
            cachedInterfaceMapping[resolvedPropertyType] = entityType
            return Pair(resolvedPropertyType, entityType)
        }

        return Pair(null, null)
    }

    private fun fetchValueEntityClassArg(property: KSPropertyDeclaration): KSType? {

        val annotation = property.annotations.first { a ->
            a.resolveName() in Constants.Annotations.Relations.relationshipAnnotations.values }

        val argChildEntityType = annotation.fetchArgValue(Constants.Annotations.Relations.Arguments.ENTITY_CLASS) as KSType?

        // if argument is not set to the default value (RoomExEntity class), take the argument
        if (argChildEntityType?.resolveTypeName().toString() != RoomExEntity::class.qualifiedName.toString())
            return argChildEntityType

        return null
    }

    private fun fetchEntityClassByInheritanceCheck(propertyType: KSType,
                                                   allEntities: Collection<KSType>): KSType? {
        val propName = propertyType.resolveTypeName().toString()
        val foundTypes: MutableSet<KSType> = mutableSetOf()

        // Loop over _every_ entity and _all_ of its supertypes.
        // This is very costly but only this way, we can be sure that an interface or base class
        // is used only once for a relation.

        // If we would not check for this scenario, we would map wrongly between entities and therefore
        // resolving the wrong children.
        for (entity in allEntities) {
            for (type in (entity.declaration as KSClassDeclaration).getAllSuperTypes()) {
                if (type.resolveTypeName().toString() == propName) {
                    foundTypes += entity
                    break
                }
            }
        }

        if (foundTypes.count() > 1)
            throw MultipleTypeUsageException("Type `${propertyType.declaration.resolveName()}` " +
                    "is used in multiple relations: ${foundTypes.joinToString { t -> t.declaration.resolveName() }}")

        return foundTypes.firstOrNull()
    }

    private class MultipleTypeUsageException(message: String) : Throwable(message)

    //endregion FETCH ENTITY TYPE

    //region RELATION TABLE NAME
    fun fetchRelationTableName(sourceEntity: KSClassDeclaration,
                               targetEntity: KSClassDeclaration,
                               sourceProperty: KSPropertyDeclaration,
                               mappedWith: String = ""): String {

        // Relationship-Entities have the following naming schema:
        // rel_{sourceEntityTableName}_{sourceProperty}_{targetEntityTableName}   in lowercase

        if (mappedWith.isBlank()) {
            return "rel_${
                fetchEntityTableName(sourceEntity)}_${
                fetchPropertyName(sourceProperty)}_${
                fetchEntityTableName(targetEntity)}"
        }
        else {
           return  "rel_${
                fetchEntityTableName(targetEntity)}_${
                mappedWith}_${
                fetchEntityTableName(sourceEntity)}"
        }
    }



    fun fetchEntityTableName(entityClass: KSClassDeclaration) : String {
        var tableName = entityClass.fetchAnnotationArgValue(Entity::class, "tableName") as String
        tableName = if (tableName.isBlank()) entityClass.resolveName(true) else tableName.trim()
        return tableName.lowercase()
    }

    fun fetchPropertyName(property: KSPropertyDeclaration) : String {
        var name = property.fetchAnnotationArgValue(Constants.Annotations.Relations.relationshipAnnotations.values.toList(),
            Constants.Annotations.Relations.Arguments.NAME) as String
        name = if (name.isBlank()) property.simpleName.getShortName() else name.trim()
        return name.lowercase()
    }
}