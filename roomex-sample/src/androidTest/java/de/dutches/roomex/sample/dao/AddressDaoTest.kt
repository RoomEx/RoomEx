package de.dutches.roomex.sample.dao

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.dutches.roomex.sample.model.Address
import de.dutches.roomex.sample.model.DummyAuto
import de.dutches.roomex.sample.model.User
import de.dutches.roomex.sample.storage.local.dao.AddressDao
import de.dutches.roomex.sample.storage.local.dao.UserDao
import de.dutches.roomex.storage.RoomExAutoDao
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class AddressDaoTest : BaseDaoTest() {

    private lateinit var userDao: UserDao
    private lateinit var dummyDao: RoomExAutoDao<DummyAuto>
    private lateinit var addressDao: AddressDao

    @Before
    fun setup() {
        userDao = db.fetchDaoForEntity(User::class) as UserDao
        addressDao = db.fetchDaoForEntity(Address::class) as AddressDao
        dummyDao = db.fetchDaoForEntity(DummyAuto::class) as RoomExAutoDao<DummyAuto>
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun insertNewAddress() = runTest {
        // Given
        val address = Address(streetname = "Markstrass", city = "Berlin")

        // When
        val id = addressDao.insert(address)

        // Then
        assertNotEquals(id, 0L)
    }

    @Test
    fun deletingAddressShouldDeleteRelations() = runTest {
        // Given
        val address = Address(streetname = "Markstrass", city = "Berlin")
        var user = User("John", "Doe")
        var dummy = DummyAuto("Sample")
        user.address = address

        // When
        // Create new user, address and dummy
        user = userDao.getByIdWithChildren(userDao.insertWithChildren(user))!!
        dummy.address = user.address
        dummy = dummyDao.getByIdWithChildren(dummyDao.insertWithChildren(dummy))!!

        // Create more sample data
        val user2 = User("Jane", "Doe")
        val user3 = User("Jane", "Smith")
        val dummy2 = DummyAuto("Sample 2")
        val dummy3 = DummyAuto("Sample 3")
        val address2 = Address("Somewhere", "?")
        user2.address = address2
        user3.address = address2
        dummy2.address = address2
        dummy3.address = address2

        userDao.insertWithChildren(user2)
        userDao.insertWithChildren(user3)
        dummyDao.insertWithChildren(dummy2)
        dummyDao.insertWithChildren(dummy3)

        // Get all current relations
        val curUserAddressRel = addressDao.getCurrentUserAddressRelations()
        val curDummyAddressRel = addressDao.getCurrentDummyAddressRelations()

        // Delete address
        addressDao.deleteWithChildren(user.address!!)

        // Get relations after delete
        val postUserAddressRel = addressDao.getCurrentUserAddressRelations()
        val postDummyAddressRel = addressDao.getCurrentDummyAddressRelations()

        // Then
        assertNotEquals(0, user.address!!.id)
        assertEquals(dummy.address!!.id, user.address!!.id)

        // Assert that the relation existed prior deleting
        assertTrue(curUserAddressRel.any { it.childId == address.id })
        assertTrue(curDummyAddressRel.any { it.childId == address.id })

        // Assert that the relation has been removed
        assertTrue(postUserAddressRel.none { it.childId == address.id })
        assertTrue(postDummyAddressRel.none { it.childId == address.id })

        // Assert that only the wanted relation has been removed
        Log.i("deletingAddressShouldDeleteRelations", "Pre user: ${curUserAddressRel.count()} -- Post user: ${postUserAddressRel.count()}")
        Log.i("deletingAddressShouldDeleteRelations", "Pre dummy: ${curDummyAddressRel.count()} -- Post dummy: ${postDummyAddressRel.count()}")
        assertEquals(curUserAddressRel.count() -1, postUserAddressRel.count())
        assertEquals(curDummyAddressRel.count() -1, postDummyAddressRel.count())
    }

    @Test
    fun getNewAddressAfterInsert() = runTest {

        // Given
        val id = addressDao.insert(Address(streetname = "Markstrass", city = "Berlin"))

        // When
        val address = addressDao.getById(id)

        // Then
        assertNotNull(address)
        assertNotEquals(address!!.id, 0L)
    }

    @Test
    fun getMappedUser() = runTest {

        // Given - Create new user with address
        val user = User(name = "John",surname = "Doe")
        user.address = Address(UUID.randomUUID().toString(), "Hamburg")
        user.id = userDao.insertWithChildren(user)
        userDao.loadChildren(user)


        // When
        val address = addressDao.getByIdWithChildren(user.address!!.id)

        // Then
        assertNotNull(user.address)
        assertNotNull(address)
        assertNotNull(address!!.user)
        assertNotEquals(address.user!!.id, 0L)
        assertEquals(address.user!!.id, user.id)
    }
}
