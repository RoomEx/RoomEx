package de.dutches.roomex.processor.extensions.kotlinpoet

import com.google.devtools.ksp.symbol.KSType
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import com.squareup.kotlinpoet.TypeName

/**
 * Recursively resolve the TypeName, including any type arguments.
 *
 * @return The resolved TypeName.
 */
internal fun KSType.resolveTypeName() : TypeName {

    // If there are no arguments (no inner types like List<String>) simple return the current type
    // Else recursively resolve the inner types
    if (this.arguments.isEmpty())
        return this.declaration.buildClassName()
    else
        this.arguments.forEach { a ->
            return if (a.type != null)
                this.declaration.buildClassName().parameterizedBy(a.type!!.resolve().resolveTypeName())
            else
                this.declaration.buildClassName()
        }

    return this.declaration.buildClassName()
}