package de.dutches.roomex.storage

import de.dutches.roomex.storage.model.RoomExEntity
import de.dutches.roomex.storage.model.relation.Cascade
import kotlin.reflect.KClass

/**
 * For internal use by RoomEx.
 * Adds usefully information about the relation properties of the entity of a Dao.
 *
 * @property type Type of the relation property, which can be a list
 * @property childEntityClass The target entity type
 * @property relationTableName Name of the relation table
 * @property mappedWith Value of the mappedWith argument from the annotation
 * @property cascade Value of the cascade argument from the annotation
 * @property childEntityTableName Name of the database table for the child entity
 */
data class EntityRelPropData(
    /** Field type */
    val type : KClass<*>,

    /** Type of the entity */
    val childEntityClass: KClass<out RoomExEntity>,

    /** The name of the relation table, so we are able to fetch all the child data */
    val relationTableName: String,

    /** Argument value of the relation annotation */
    val mappedWith : String = "",

    /** Argument value of the relation annotation */
    val cascade: Cascade = Cascade.None,

    /** The name of the child entity table */
    val childEntityTableName : String = ""
)
