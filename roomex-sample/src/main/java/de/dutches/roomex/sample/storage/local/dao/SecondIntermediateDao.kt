package de.dutches.roomex.sample.storage.local.dao

import de.dutches.roomex.storage.model.RoomExEntity

abstract class SecondIntermediateDao<T : RoomExEntity> : IntermediateDao<T>() { }