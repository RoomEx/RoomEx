@file:Suppress("unused")
package de.dutches.roomex.storage.crud

import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Interface to add public methods to your Dao to read
 * entities from the database.
 *
 * @param T Entity type
 */
interface IRead<T> where T : RoomExEntity {

    /**
     * Check whether an entity with the given id exists.
     *
     * @param id Id of the entity to check for
     * @return True, if entity exists
     */
    suspend fun exists(id: Long): Boolean

    /**
     * Get the entity with the given id.
     *
     * @param id Primary key of the entity.
     * @return The found entity or null.
     */
    suspend fun getById(id: Long): T?

    /**
     * Get all entities from the database.
     *
     * @return All entities in the database.
     */
    suspend fun getAll(): MutableList<T>

    /**
     * Get the entity with the given id including its children.
     *
     * @param id Primary key of the entity.
     * @param recursively Should children be fetched recursively.
     * @param fields The field name of any child fields to load.
     * @return The found entity with its children or null.
     */
    suspend fun getByIdWithChildren(id: Long, recursively: Boolean = false, vararg fields: String) : T?

    /**
     * Load children for the given entity.
     * The children are directly added to the entity, overwriting any existing value.
     *
     * @param entity The entity for which the children should be loaded.
     * @param recursively Should children be fetched recursively.
     * @return The entity with its children.
     */
    suspend fun loadChildren(entity: T, recursively: Boolean = false) : T

    /**
     * Load children for the given entity.
     * The children are directly added to the entity, overwriting any existing value.
     *
     * @param entity The entity for which the children should be loaded.
     * @param recursively Should children be fetched recursively.
     * @param fields The field names of the children to load.
     * @return The entity with its children.
     */
    suspend fun loadChildren(entity: T, recursively: Boolean = false, vararg fields : String) : T
}