package de.dutches.roomex.processor

import de.dutches.roomex.storage.model.relation.ManyToManyEx
import de.dutches.roomex.storage.model.relation.ManyToOneEx
import de.dutches.roomex.storage.model.relation.OneToManyEx
import de.dutches.roomex.storage.model.relation.OneToOneEx

internal object Constants {

    object Annotations {
        object Relations {
            object Arguments {
                const val MAPPED_WITH = "mappedWith"
                const val ENTITY_CLASS = "entityKClass"
                const val CASCADE = "cascade"
                const val NAME = "name"
            }

            internal val relationshipAnnotations = mapOf(
                OneToOneEx::class to OneToOneEx::class.qualifiedName!!,
                OneToManyEx::class to OneToManyEx::class.qualifiedName!!,
                ManyToOneEx::class to ManyToOneEx::class.qualifiedName!!,
                ManyToManyEx::class to ManyToManyEx::class.qualifiedName!!
            )
        }

        object AutoDao {
            object Arguments {
                const val PACKAGE_NAME = "packageName"
            }
        }

        object RoomExDb {
            object Arguments {
                const val VERSION = "version"
                const val EXPORT_SCHEMA = "exportSchema"
            }
        }
    }


    internal const val roomExPackageUrl                         = "de.dutches"
    internal const val roomExPackageProject                     = "$roomExPackageUrl.roomex"
    internal const val roomExPackageStorage                     = "$roomExPackageProject.storage"
    internal const val roomExPackageCrud                        = "$roomExPackageStorage.crud"
    internal const val roomExPackageModel                       = "$roomExPackageStorage.model"
    internal const val roomExPackageRelations                   = "$roomExPackageModel.relation"

    internal const val roomExEntityClassName                    = "$roomExPackageModel.RoomExEntity"
    internal const val roomExDaoClassName                       = "$roomExPackageStorage.RoomExDao"
    internal const val roomExAutoDaoClassName                   = "$roomExPackageStorage.RoomExAutoDao"
    internal const val roomExInternalDaoInterfaceName           = "$roomExPackageStorage.IRoomExInternalDao"
    internal const val roomExInternalDaoRawQueriesInterfaceName = "$roomExPackageStorage.IRoomExInternalDaoRawQueries"
    internal const val roomExDaoInterfaceName                   = "$roomExPackageStorage.IRoomExDao"
    internal const val roomExDatabaseClassName                  = "$roomExPackageStorage.RoomExDatabase"
    internal const val roomExDbBaseClassName                    = "$roomExPackageStorage.RoomExDbBase"

    internal const val roomExCrudCreateInterfaceName            = "$roomExPackageCrud.ICreate"
    internal const val roomExCrudReadInterfaceName              = "$roomExPackageCrud.IRead"
    internal const val roomExCrudUpdateInterfaceName            = "$roomExPackageCrud.IUpdate"
    internal const val roomExCrudDeleteInterfaceName            = "$roomExPackageCrud.IDelete"
    internal const val roomExCrudJoinsInterfaceName             = "$roomExPackageCrud.IJoins"
}