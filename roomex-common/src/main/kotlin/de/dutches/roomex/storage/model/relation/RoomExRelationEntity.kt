package de.dutches.roomex.storage.model.relation

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

open class RoomExRelationEntity (

    @ColumnInfo(name = PARENTFIELDNAME)
    var parentId: Long,

    @ColumnInfo(name = CHILDFIELDNAME)
    var childId: Long
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0L

    companion object{
        const val PARENTFIELDNAME = "parentId"
        const val CHILDFIELDNAME = "childId"
    }
}