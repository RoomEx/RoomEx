
plugins {
    kotlin("jvm")
}
buildscript {
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.7.21")
        classpath("com.android.tools.build:gradle:7.3.1")
        classpath("com.google.dagger:hilt-android-gradle-plugin:2.39.1")
    }
}

group = getSetting("roomExGroupName")
version = "${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}"

repositories {
    mavenCentral()
    google()
}

fun getSetting(key: String) = project.getSetting(key)
fun Project.getSetting(key: String): String {
    if (!project.properties.containsKey(key))
        throw Exception("Key $key not found in gradle.properties")
    return project.properties[key].toString()
}