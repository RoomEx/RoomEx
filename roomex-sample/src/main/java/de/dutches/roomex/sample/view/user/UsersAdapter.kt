package de.dutches.roomex.sample.view.user

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.dutches.roomex.sample.databinding.ItemUserBinding
import de.dutches.roomex.sample.model.User

class UsersAdapter() : ListAdapter<User, UsersAdapter.UsersViewHolder>(DiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder
            = UsersViewHolder(ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false))


    override fun onBindViewHolder(holder: UsersViewHolder, position: Int)
            = holder.bind(getItem(position))

    inner class UsersViewHolder(private val binding: ItemUserBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        fun bind(user: User) {
            binding.apply {
                userTextUsername.text = user.name
            }
        }

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            // no action needed
        }
    }

    class DiffCallBack : DiffUtil.ItemCallback<User>() {

        override fun areItemsTheSame(oldItem: User, newItem: User)
                = oldItem.id  == newItem.id

        override fun areContentsTheSame(oldItem: User, newItem: User)
                = oldItem == newItem
    }

    interface OnItemClickListener {
        fun onItemClick(user: User)
    }
}