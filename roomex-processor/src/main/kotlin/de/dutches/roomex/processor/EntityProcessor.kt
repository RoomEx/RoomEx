package de.dutches.roomex.processor

import androidx.room.*
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.Dependencies
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.symbol.*
import com.squareup.kotlinpoet.*
import de.dutches.roomex.processor.extensions.ksp.*
import de.dutches.roomex.processor.helper.relations.RelationBuilder
import de.dutches.roomex.processor.helper.relations.RelationUtils.fetchEntityTypeForProperty
import de.dutches.roomex.processor.helper.relations.RelationUtils.fetchRelationTableName
import de.dutches.roomex.processor.relation.RelationValidationProcessor

/**
 * The annotation processor which handles the [Entity] annotation.
 * Iterates over every entity class and creates new relationship entities where needed.
 * Note: Validation of the relationships and its properties is done in the [RelationValidationProcessor]
 *
 * @param codeGenerator
 * @param logger
 * @param options
 */
class EntityProcessor(
    codeGenerator: CodeGenerator,
    logger: KSPLogger,
    options: Map<String, String>
) : AbstractProcessor(codeGenerator, logger, options) {

    override val annotationClass = Entity::class.java
    override fun getVisitor() = EntityVisitor()

    protected inner class EntityVisitor : AbstractProcessor.Visitor() {

        lateinit var entityClass: KSClassDeclaration
        /**
         * Processes the given [Entity]-class and checks for relations.
         * For each given relation, minus inverse relations, a new entity will be created.
         *
         * @param classDeclaration
         * @param data
         */
        override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit) {
            entityClass = classDeclaration
            super.visitClassDeclaration(classDeclaration, data)
            classDeclaration.getAllProperties().forEach { p -> p.accept(this, data) }
        }

        override fun visitPropertyDeclaration(property: KSPropertyDeclaration, data: Unit) {
            super.visitPropertyDeclaration(property, data)

            // Abort if no relation annotation exists
            if (!property.isAnnotationPresent(Constants.Annotations.Relations.relationshipAnnotations.keys.toTypedArray()))
                return

            // No further validation needed, since that has been handled by the RelationValidationProcessors

            val (_, childEntityType) = fetchEntityTypeForProperty(property,
                CollectorProcessor.allEntitiesWithRelations.keys,
                CollectorProcessor.entityInterfaceMapping)
            if (childEntityType == null) {
                logger.error("Unable to resolve property ${property.simpleName.asString()} to an entity.", property)
                return
            }

            // Get value of "mappedWith"
            // Note: Should mappedWith not be empty, the relation table of the mapped field is used and the usage
            // of the parent- and childids are reverse
            val mappedWith = property
                .fetchAnnotationArgValue(Constants.Annotations.Relations.relationshipAnnotations.values.toList(),
                    Constants.Annotations.Relations.Arguments.MAPPED_WITH) as String

            val relTableName = fetchRelationTableName(entityClass,
                childEntityType.declaration as KSClassDeclaration,
                property,
                mappedWith)

            // Build the relationship entity if needed
            if (mappedWith.isEmpty()) {
                createRelationEntity(relTableName)
            }
            // Else: Do not create the relationship entity as it will/has been created by the original relation

            // Add the relationship to the collector (for parent and child entity)
            CollectorProcessor.allEntitiesWithRelations[entityClass.asStarProjectedType()]!! += Pair(relTableName, mappedWith.isEmpty())
            CollectorProcessor.allEntitiesWithRelations[childEntityType.makeNotNullable()]!! += Pair(relTableName, mappedWith.isNotEmpty())

        }

        private fun createRelationEntity(relTableName: String) {

            // Create the relation entity type
            val relClas = RelationBuilder(relTableName).build().build()

            logger.info("Relationship found and new entity has been been build: $relTableName")

            val relationSourceFile = FileSpec
                .builder(Constants.roomExPackageRelations, relTableName.replaceFirstChar { c -> c.uppercaseChar() })

            relationSourceFile.addType(relClas).build()

            if (entityClass.containingFile != null)
                addNewCodeFile(Constants.roomExPackageRelations,
                    relTableName.replaceFirstChar { c -> c.uppercaseChar() },
                    relationSourceFile.build().toString(),
                    Dependencies(false, entityClass.containingFile!!)
                )
            else
                addNewCodeFile(Constants.roomExPackageRelations,
                    relTableName.replaceFirstChar { c -> c.uppercaseChar() },
                    relationSourceFile.build().toString()
                )
        }
    }
}