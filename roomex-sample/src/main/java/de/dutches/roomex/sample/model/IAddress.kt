package de.dutches.roomex.sample.model

interface IAddress : IBaseEntity {
    var streetname: String
    var city: String
    var user : User?
    var contact : Contact?
}