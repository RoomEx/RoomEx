package de.dutches.roomex.processor

import androidx.room.Dao
import androidx.room.Database
import com.google.devtools.ksp.getAllSuperTypes
import com.google.devtools.ksp.isAbstract
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.Dependencies
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.symbol.ClassKind
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSDeclaration
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import de.dutches.roomex.processor.CollectorProcessor.ResolvedSymbols.allDaoWithEntity
import de.dutches.roomex.processor.CollectorProcessor.ResolvedSymbols.allEntitiesWithRelations
import de.dutches.roomex.processor.extensions.kotlinpoet.buildClassName
import de.dutches.roomex.processor.extensions.ksp.fetchAnnotationArgValue
import de.dutches.roomex.processor.extensions.ksp.resolveName
import de.dutches.roomex.storage.AutoDao
import de.dutches.roomex.storage.DaoEx
import de.dutches.roomex.storage.RoomExDb
import de.dutches.roomex.storage.model.RoomExEntity
import kotlin.reflect.KClass

/**
 * The annotation processor which handles the [DaoEx] annotation.
 *
 * @param codeGenerator
 * @param logger
 * @param options
 */
class RoomExDbProcessor(
    codeGenerator: CodeGenerator,
    logger: KSPLogger,
    options: Map<String, String>
) : AbstractProcessor(codeGenerator, logger, options) {

    // Instance is kept alive between runs, therefore we are able to store data between runs
    private var finished = false

    private val targetClassName = Constants.roomExDatabaseClassName.removePrefix(Constants.roomExPackageStorage + ".")

    override val annotationClass = RoomExDb::class.java
    override fun getVisitor() = RoomExDbVisitor()

    override fun runVisitor(validSymbols: List<KSDeclaration>) : List<KSDeclaration> {

        if (!autoDaoAndDaoExProcessorFinished()) {
            finished = false
            return validSymbols
        }

        if (finished)
            return listOf()

        // We now know the DaoExProcessor has finished, so we can process any Db
        // Should there be no Db-class, we simply create one
        if (validSymbols.isEmpty()) {
            createDbClass()
        }

        return super.runVisitor(validSymbols)
    }

    private fun autoDaoAndDaoExProcessorFinished()
        // Processors are done, when no Dao- or DaoEx-class are resolvable
        = resolver
            .getSymbolsWithAnnotation(DaoEx::class.java.name)
            .filterIsInstance<KSDeclaration>()
            .none()
            && resolver
                .getSymbolsWithAnnotation(AutoDao::class.java.name)
                .filterIsInstance<KSDeclaration>()
                .none()
            && resolver
                .getSymbolsWithAnnotation(Dao::class.java.name)
                .filterIsInstance<KSDeclaration>()
                .any() // Don't create a database file when there are no Dao's

    fun createDbClass(superClass: KSClassDeclaration? = null) {

        val file = FileSpec.builder(Constants.roomExPackageStorage, targetClassName)
        val builder = TypeSpec.classBuilder(targetClassName)

        // Annotation arguments for @Database
        var version = 1
        var exportSchema = false

        // Make the class abstract
        builder.addModifiers(KModifier.ABSTRACT)

        // Import and set super class/interface
        file.addImport("androidx.room", "RoomDatabase")
        allDaoWithEntity.forEach {
            file.addImport(it.key.packageName.asString(), it.key.simpleName.getShortName())
        }
        allEntitiesWithRelations.keys.forEach {
            file.addImport(it.declaration.packageName.asString(), it.declaration.simpleName.getShortName())
        }

        if (superClass != null) {
            file.addImport(superClass.packageName.asString(), superClass.simpleName.asString())
            version = superClass.fetchAnnotationArgValue(RoomExDb::class, Constants.Annotations.RoomExDb.Arguments.VERSION) as Int
            exportSchema = superClass.fetchAnnotationArgValue(RoomExDb::class, Constants.Annotations.RoomExDb.Arguments.EXPORT_SCHEMA) as Boolean
        }

        builder.addAnnotation(
            AnnotationSpec
                .builder(Database::class)
                .addMember("entities = [${allEntitiesWithRelations.keys.joinToString { e -> e.declaration.simpleName.getShortName() + "::class" }}]")
                .addMember("version = $version")
                .addMember("exportSchema = $exportSchema")
                .build()
        )

        // Implement any method inherited by any RoomEx interface or class
        val funSpecs = implementMethods()

        // Implement any property inherited by any RoomEx interface or class
        val propSpecs = implementProperties()

        // Everything created, build the final class
        builder.addFunctions(funSpecs)
        builder.addProperties(propSpecs)

        // Build the final class
        file.addType(builder.build())

        var dbFileContent = file.build().toString()

        // Add the RoomDatabase class as super class
        // Package with the RoomDatabase class cannot be added as a dependency, since it is an android package
        // Therefore we need to add it manually, post class generation
        dbFileContent = StringBuilder(dbFileContent)
            .insert(dbFileContent.indexOf(targetClassName) + targetClassName.length, " : ${Constants.roomExDbBaseClassName}() ").toString()


        // Write the new database class to a file
        addNewCodeFile(Constants.roomExPackageStorage,
            targetClassName,
            dbFileContent,
            Dependencies(false, *resolver.getAllFiles().toList().toTypedArray())
        )
        finished = true
    }

    private fun implementMethods(): List<FunSpec> {

        val funSpecs = mutableListOf<FunSpec>()
        val roomExEntityTypeVar = TypeVariableName("T", RoomExEntity::class)


        // Implement the DAO-instantiating methods
        for (daoWithEntity in allDaoWithEntity) {
            funSpecs += FunSpec
                .builder(daoWithEntity.key.buildClassName().simpleName.replaceFirstChar { c -> c.lowercase() })
                .addModifiers(KModifier.ABSTRACT)
                .returns(daoWithEntity.key.buildClassName())
                .build()
        }

        // Implement the method to easily fetch a dao for a given entity
        val dbDaoFetcherFunBuilder = FunSpec
            .builder("fetchDaoForEntity")
            .addTypeVariable(roomExEntityTypeVar)
            .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE)
            .returns(ClassName.bestGuess(Constants.roomExDaoClassName).parameterizedBy(roomExEntityTypeVar))
            .addParameter(ParameterSpec("entityKClass", KClass::class.buildClassName().parameterizedBy(roomExEntityTypeVar)))
            .addAnnotation(AnnotationSpec.builder(Suppress::class).addMember("\"unchecked_cast\"").build())
            .beginControlFlow("return when(entityKClass)")

        allDaoWithEntity.forEach { d ->
            dbDaoFetcherFunBuilder.addStatement("${d.value.declaration.resolveName(true)}::class -> " +
                    "${d.key.resolveName(true).replaceFirstChar { c -> c.lowercase() }}() " +
                    "as ${Constants.roomExDaoClassName}<T>")
        }

        funSpecs += dbDaoFetcherFunBuilder
            .addStatement("else -> throw IllegalArgumentException(\"An invalid entity class has been passed: \" + entityKClass.qualifiedName)")
            .endControlFlow()
            .build()

        return funSpecs
    }



    private fun implementProperties(): List<PropertySpec> {
        return mutableListOf()
    }
    protected inner class RoomExDbVisitor : AbstractProcessor.Visitor() {

        /**
         * Processes the given [RoomExDb]-class and creates the final database class for Room.
         *
         * @param classDeclaration
         * @param data
         */
        override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit) {
            // Super handles the creation of the FileBuilder
            super.visitClassDeclaration(classDeclaration, data)

            createDbClass(classDeclaration)
        }

         /**
         * Checks if the given class declaration is valid.
         *
         * Should the class be invalid, a message is added to the log.
         *
         * @param declaration The class to validate
         * @return True if valid, false if not
         */
        override fun checkAndLogClassValidation(declaration : KSClassDeclaration) : Boolean {
             var isValid = true

             // Check if set on an abstract class
             if (declaration.classKind != ClassKind.CLASS || !declaration.isAbstract())
             {
                 logger.error("Annotation \"${RoomExDb::class.simpleName}\" can only be used on abstract classes.", declaration)
                 isValid = false
             }

             // Check if it is the only one
             if (resolver.getSymbolsWithAnnotation(annotationClass.name).count() != 1)
             {
                 logger.error("Annotation \"${RoomExDb::class.simpleName}\" can only be used once.", declaration)
                 isValid = false
             }

             // Check if extends RoomExDbBase
             if (declaration.getAllSuperTypes().none { s -> s.declaration.resolveName() == Constants.roomExDbBaseClassName })
             {
                 logger.error("Database class must implement: ${Constants.roomExDbBaseClassName}.", declaration)
                 isValid = false
             }

             return isValid
        }
    }
}