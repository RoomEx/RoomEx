package de.dutches.roomex.processor

import com.google.devtools.ksp.getDeclaredFunctions
import com.google.devtools.ksp.getDeclaredProperties
import com.google.devtools.ksp.processing.*
import com.google.devtools.ksp.symbol.*
//KSAnnotated
//import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.validate
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.PropertySpec
import de.dutches.roomex.processor.extensions.ksp.isAnnotationPresent
import kotlin.reflect.KClass

abstract class AbstractProcessor(
    private val codeGenerator: CodeGenerator,
    protected val logger: KSPLogger,
    protected val options: Map<String, String>
): SymbolProcessor {

    /** Resolver which can be used to fetch symbols */
    protected lateinit var resolver: Resolver
    /** Contains all declarations with the given annotation */
    lateinit var symbols: List<KSDeclaration>
    /** Annotation which is targeted by this processor */
    protected abstract val annotationClass : Class<out Annotation>

    override fun process(resolver: Resolver): List<KSAnnotated> {
        this.resolver = resolver

        // Get all declarations with the given annotation
        symbols = fetchSymbols()

        // Loop with the visitor over every class
        val unfinished = runVisitor(symbols.filter { it.validate() })

        logger.info("Processor for ${annotationClass.name} finished")

        // return all currently invalid symbols
        // they will be processed in one of the next rounds
        return symbols.filterNot { it.validate() }.plus(unfinished)
    }

    protected open fun fetchSymbols()
        = resolver
            .getSymbolsWithAnnotation(annotationClass.name)
            .filterIsInstance<KSDeclaration>()
            .toList()

    protected open fun runVisitor(validSymbols: List<KSDeclaration>): List<KSDeclaration> {
        if (validSymbols.isEmpty())
            return validSymbols

        validSymbols.forEach { s -> s.accept(getVisitor(), Unit) }

        return listOf()
    }

    protected open fun getVisitor() : AbstractProcessor.Visitor = Visitor()
    protected open fun postOperations() = run { }

    protected open inner class Visitor : KSVisitorVoid() {

        protected lateinit var builderFile : FileSpec.Builder
        protected open val classPostfix : String = "Ex"

        /**
         * Checks if the given class declaration is valid.
         * Any error should be reported with [error].
         *
         * @param declaration The class to validate
         * @return True if valid, false if not
         */
        open fun checkAndLogClassValidation(declaration : KSClassDeclaration): Boolean = true

        override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit) {
            super.visitClassDeclaration(classDeclaration, data)

            // Abort operations on this class if class is not valid
            if (!checkAndLogClassValidation(classDeclaration))
                return

            // Create the initial class document
            builderFile = FileSpec.builder(classDeclaration.packageName.asString(),
                classDeclaration.simpleName.asString() + classPostfix)
        }

        /**
         * Retrieve all declared properties for the given class, which are annotated with at least one of the given annotations
         *
         * @param classDeclaration The class for which the properties should be retrieved for
         * @param annotations List of valid annotations
         * @return Sequence with the found properties
         */
        protected fun fetchProperties(classDeclaration: KSClassDeclaration, annotations : Array<KClass<out Annotation>>) : Sequence<KSPropertyDeclaration> {
            return classDeclaration.getDeclaredProperties()
                .filter { it.isAnnotationPresent(annotations) }
                .filter { it.validate() }
        }

        /**
         * Retrieve all declared functions for the given class, which are annotated with at least one of the given annotations
         *
         * @param classDeclaration The class for which the functions should be retrieved for
         * @param annotations List of valid annotations
         * @return Sequence with the found functions
         */
        protected fun fetchFunctions(classDeclaration: KSClassDeclaration, annotations : Array<KClass<out Annotation>>) : Sequence<KSFunctionDeclaration> {
            return classDeclaration.getDeclaredFunctions()
                .filter { it.isAnnotationPresent(annotations) }
                .filter { it.validate() }
        }

        protected open fun implementMethods() : List<FunSpec> = mutableListOf()

        protected open fun implementProperties() : List<PropertySpec> = mutableListOf()
    }

    /**
     * Creates a new source file using the code generator
     *
     * @param packageName The package name of the generated class / file
     * @param fileName The name of the output file (will be prepended with .kt)
     * @param content The content which should be written to the output file
     * @param dependencies Any dependencies for this file
     */
    protected fun addNewCodeFile(packageName: String,
                                 fileName: String,
                                 content: String,
                                 dependencies: Dependencies = Dependencies(false)) {

        logger.info("Creating new source file: $fileName -- ${this.javaClass.name}")
        val outPutFile = codeGenerator.createNewFile(
            dependencies = dependencies,
            packageName = packageName,
            fileName = fileName
        )

        outPutFile.write(content.toByteArray())
        outPutFile.close()
    }

    protected fun addNewCodeFile(fileSpec: FileSpec,
                                 dependencies: Dependencies = Dependencies(false)) {

        return addNewCodeFile(fileSpec.packageName, fileSpec.name, fileSpec.toString(), dependencies)
    }
}