package de.dutches.roomex.sample.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import de.dutches.roomex.storage.model.RoomExEntity
import de.dutches.roomex.storage.model.relation.Cascade
import de.dutches.roomex.storage.model.relation.OneToOneEx

// Since this is only a sample class, suppress some warnings
@Suppress("unused")
@Entity(tableName = "adress")
data class Address(
    @ColumnInfo(name = "streetname")
    override var streetname: String = "Musterstrasse",

    @ColumnInfo(name = "city")
    override var city: String = "Erfurt"

) : RoomExEntity(), IAddress {

    @Ignore
    @OneToOneEx("address", cascade = Cascade.All)
    override var user : User? = null

    @Ignore
    @OneToOneEx(cascade = Cascade.All)
    override var contact : Contact? = null

    override fun toString(): String {
        return """${System.lineSeparator()}
        Address: $id --> $streetname, $city
        User: $user
        """
    }
}

@Suppress("unused")
@Entity(tableName = "dummyadress")
data class DummyAddress(
//    override var streetname: String,
//    override var city: String,
//    override var user: User?,
//    override var contact: Contact?
    var dummy: String

) : RoomExEntity()//, IAddress