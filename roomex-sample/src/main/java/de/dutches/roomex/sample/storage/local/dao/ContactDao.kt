package de.dutches.roomex.sample.storage.local.dao

import de.dutches.roomex.sample.model.Contact
import de.dutches.roomex.storage.DaoEx
import de.dutches.roomex.storage.crud.ICrud

@DaoEx
abstract class ContactDao : SecondIntermediateDao<Contact>(), ICrud<Contact>