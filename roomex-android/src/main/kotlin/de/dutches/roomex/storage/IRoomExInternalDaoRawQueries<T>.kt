package de.dutches.roomex.storage

import androidx.room.RawQuery
import androidx.sqlite.db.SupportSQLiteQuery
import de.dutches.roomex.storage.model.relation.RoomExRelationEntity

/**
 * For internal use by RoomEx.
 * Adds needed RawQueries to the Dao, which will be implemented by Room.
 *
 * @param T The entity type
 */
interface IRoomExInternalDaoRawQueries<T> {

    @RawQuery
    suspend fun joinQuery(query: SupportSQLiteQuery) : Long

    @RawQuery
    suspend fun joinFetchQuery(query: SupportSQLiteQuery) : MutableList<RoomExRelationEntity>

    @RawQuery
    suspend fun fetchChildrenQuery(query: SupportSQLiteQuery): MutableList<T>

    // Note: Return type of Int is only used to trick Room to allow @RawQuery, its actual value will always be null
    @RawQuery
    suspend fun deleteQuery(query: SupportSQLiteQuery): Int
}