package de.dutches.roomex.extensions

/**
 * Returns null if the list is empty, otherwise the original list.
 *
 * @return Original list or null
 */
internal fun <T> List<T>?.emptyToNull(): List<T>? {
    return if (this.isNullOrEmpty()) null else this
}