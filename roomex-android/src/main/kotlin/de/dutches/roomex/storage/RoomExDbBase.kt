package de.dutches.roomex.storage

import androidx.room.RoomDatabase
import de.dutches.roomex.storage.model.RoomExEntity
import kotlin.reflect.KClass

/**
 * Base class for the RoomExDatabase which will be created during compilation.
 *
 */
abstract class RoomExDbBase : RoomDatabase() {

    /**
     * Retrieves the DAO which corresponds to the provided entity class
     *
     * @param T Entity of the DAO
     * @param entityKClass Entity class of the DAO
     * @return Dao for the given entity. If no DAO exists, an error is thrown.
     */
    abstract fun <T : RoomExEntity> fetchDaoForEntity(entityKClass: KClass<T>): RoomExDao<T>

    companion object {

        /**
         * Retrieves the RoomExDatabase class, which is generated during compilation.
         * This class can be used for creating a Room database.
         *
         * @return
         */
        fun getDbClass(): Class<out RoomDatabase> = Class.forName("de.dutches.roomex.storage.RoomExDatabase").asSubclass(RoomDatabase::class.java)
    }
}