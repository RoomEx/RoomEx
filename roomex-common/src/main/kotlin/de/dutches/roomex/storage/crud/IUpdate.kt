@file:Suppress("unused")
package de.dutches.roomex.storage.crud

import androidx.room.Update
import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Interface to add public methods to your Dao to update
 * existing entities in the database.
 *
 * @param T Entity type
 */
interface IUpdate<T> where T : RoomExEntity {

    /**
     * Update the given entity in the database.
     *
     * @param entity The entity to update.
     */
    @Update
    suspend fun update(entity: T)

    /**
     * Updates the given entity and its relations in the database.
     *
     * @param entity The entity to update in the database.
     * @param removeIfNull Determines if relation fields which are set to null should be processed or not.
     * @param removeFromList Determines if list relation fields should be processed for disjoining.
     * @param recursively Should children be updated recursively.
     */
    suspend fun updateWithChildren(entity: T, removeIfNull: Boolean = false, removeFromList: Boolean = true, recursively: Boolean = false)

    /**
     * Updates the given entity and its relations in the database.
     *
     * @param entity The entity to update in the database.
     * @param removeIfNull Determines if relation fields which are set to null should be processed or not.
     * @param removeFromList Determines if list relation fields should be processed for disjoining.
     * @param recursively Should children be updated recursively.
     * @param fields The relation fields which should be processed.
     */
    suspend fun updateWithChildren(entity: T, removeIfNull: Boolean = false, removeFromList: Boolean = true, recursively: Boolean = false, vararg fields: String)
}