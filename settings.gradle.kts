pluginManagement {
    val kotlinVersion: String by settings
    val kspVersion: String by settings

    plugins {
        id("com.google.devtools.ksp") version kspVersion
        kotlin("jvm") version kotlinVersion
    }
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}


rootProject.name = "RoomEx"

include(":roomex-android")
include(":roomex-processor")
include(":roomex-sample")
include(":roomex-common")
