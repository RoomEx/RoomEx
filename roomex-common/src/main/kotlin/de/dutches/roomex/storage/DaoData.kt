package de.dutches.roomex.storage

import de.dutches.roomex.storage.model.RoomExEntity
import kotlin.reflect.KClass

/**
 * For internal use by RoomEx.
 * Adds usefully information about the entity of a Dao.
 *
 * @property entityClass KClass of the entity
 * @property entityTableName Name of the database table
 * @property relations All relations from and to this entity
 */
data class DaoData(

    /** Entity for this Dao */
    val entityClass : KClass<out RoomExEntity>,

    /** Database table name for this entity */
    val entityTableName: String,

    /** All ingoing and outgoing relations
     * Boolean is True when outgoing relation */
    val relations: List<Pair<String, Boolean>> = listOf()
)
