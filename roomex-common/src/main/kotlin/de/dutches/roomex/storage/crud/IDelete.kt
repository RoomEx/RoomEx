@file:Suppress("unused")
package de.dutches.roomex.storage.crud

import androidx.room.Delete
import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Interface to add public methods to your Dao to delete
 * existing entities from the database.
 *
 * @param T Entity type
 */
interface IDelete<T> where T : RoomExEntity {
    /**
     * Deletes the given entity from the database.
     * Note: Relations will not be altered.
     *
     * @param entity The entity to remove.
     */
    @Delete
    suspend fun delete(entity: T): Int

    /**
     * Deletes the entity with the given id from the database.
     * Note: Relations will not be altered.
     *
     * @param id The id of the entity to remove.
     */
    suspend fun delete(id: Long)

    /**
     * Deletes the entity and its children with the "deleteCascade" flag from the database.
     * If no child field is given, all children with the "deleteCascade" flag will be deleted.
     *
     * @param entity The entity for which the relations should be updated.
     * @param fields The relation fields which should be processed.
     * @param recursively Should children be deleted recursively.
     *
     * @throws [IllegalArgumentException] If given relation field does not exist.
     * @throws [IllegalArgumentException] If entity id is not set.
     * @throws [IllegalArgumentException] If any given field is not marked with "deleteCascade = true".
     */
    suspend fun deleteWithChildren(entity: T, recursively: Boolean = false, vararg fields : String): Int

    /**
     * Deletes children with the "deleteCascade" flag from the database.
     * If no child field is given, all children with the "deleteCascade" flag will be deleted.
     *
     * @param entity The entity for which the relations should be updated.
     * @param recursively Should children be deleted recursively.
     * @param clearFromEntity If the relation fields should be cleared from the parent (children set to null).
     * @param fields The relation fields which should be processed.
     *
     * @throws [IllegalArgumentException] If given relation field does not exist.
     * @throws [IllegalArgumentException] If entity id is not set.
     * @throws [IllegalArgumentException] If any given field is not marked with "deleteCascade = true".
     */
    suspend fun deleteChildren(entity: T, recursively: Boolean = false, clearFromEntity: Boolean = false, vararg fields : String)
}