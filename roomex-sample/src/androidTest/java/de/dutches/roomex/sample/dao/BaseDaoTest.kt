package de.dutches.roomex.sample.dao

import androidx.test.platform.app.InstrumentationRegistry
import androidx.room.Room
import de.dutches.roomex.storage.RoomExDbBase

abstract class BaseDaoTest {
    var db : RoomExDbBase = Room.inMemoryDatabaseBuilder(
        InstrumentationRegistry.getInstrumentation().targetContext,
        RoomExDbBase.getDbClass()
    ).allowMainThreadQueries()
        .build() as RoomExDbBase
}