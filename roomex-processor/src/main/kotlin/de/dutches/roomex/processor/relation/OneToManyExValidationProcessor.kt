package de.dutches.roomex.processor.relation

import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.symbol.KSPropertyDeclaration
import de.dutches.roomex.processor.extensions.ksp.isAnnotationPresent
import de.dutches.roomex.storage.model.RoomExEntity
import de.dutches.roomex.storage.model.relation.ManyToManyEx
import de.dutches.roomex.storage.model.relation.ManyToOneEx
import de.dutches.roomex.storage.model.relation.OneToManyEx
import de.dutches.roomex.storage.model.relation.OneToOneEx
import kotlin.reflect.KClass

class OneToManyExValidationProcessor(
    codeGenerator: CodeGenerator,
    logger: KSPLogger,
    options: Map<String, String>
) : RelationValidationProcessor(codeGenerator, logger, options) {

    override val annotationClass = OneToManyEx::class.java
    override val allowedMappedAnnotations: Array<KClass<out Annotation>> = arrayOf(ManyToOneEx::class)
    override fun getVisitor() = OneToManyVisitor()

    protected inner class OneToManyVisitor : RelationVisitor() {

        override fun visitPropertyDeclaration(property: KSPropertyDeclaration, data: Unit) {
            val resolvedType = property.type.resolve()

            // Make sure that the annotation is used on a valid field
            if (resolvedType.arguments.isEmpty() || resolvedType.arguments.count() > 1)
                logger.error("Annotation ${OneToManyEx::class.simpleName} can only be used on a list collection.",property)
            else if (!checkTargetChildEntity(property, resolvedType))
                logger.error("Annotation ${OneToManyEx::class.simpleName} can only be used with entities implementing ${RoomExEntity::class.simpleName}.",property)

            // Check that there are no further roomex annotations
            if (property.isAnnotationPresent(arrayOf(OneToOneEx::class, ManyToManyEx::class, ManyToOneEx::class)))
                logger.error("Annotation ${OneToManyEx::class.simpleName} cannot be combined with other relation annotations.",property)

            // Check @Ignore, @Entity, nullability and mutability in the base class
            super.visitPropertyDeclaration(property, data)
        }
    }
}