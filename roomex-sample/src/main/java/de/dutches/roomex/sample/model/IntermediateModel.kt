package de.dutches.roomex.sample.model

import de.dutches.roomex.storage.model.RoomExEntity

abstract class IntermediateModel<T : RoomExEntity>(id: Long) : BaseModel(id) {
}