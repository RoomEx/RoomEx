package de.dutches.roomex.sample.storage.local

import de.dutches.roomex.storage.RoomExDao
import de.dutches.roomex.storage.RoomExDb
import de.dutches.roomex.storage.RoomExDbBase
import de.dutches.roomex.storage.model.RoomExEntity
import kotlin.reflect.KClass

@Suppress("unused")
@RoomExDb
abstract class Db : RoomExDbBase() {

    // Sample method...
    fun retrieveDoasForEntities(entityKlasses: List<KClass<out RoomExEntity>>): List<RoomExDao<out RoomExEntity>> {
        return entityKlasses.map { e -> this.fetchDaoForEntity(e)  }
    }
}