package de.dutches.roomex.processor.helper.relations

import androidx.room.Entity
import com.squareup.kotlinpoet.AnnotationSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeSpec
import de.dutches.roomex.storage.model.relation.RoomExRelationEntity

/**
 * Handles the generation of a new relation entity.
 *
 * @property tableName The table name to use for this entity.
 */
class RelationBuilder(private var tableName: String) {
    fun build() : TypeSpec.Builder {
        return TypeSpec
            .classBuilder(tableName)
            .addAnnotation(AnnotationSpec.builder(Entity::class)
                .addMember("tableName = \"${tableName}\"")
                .build())
            .superclass(RoomExRelationEntity::class)
            .primaryConstructor(FunSpec
                .constructorBuilder()
                .addParameter("parentId", Long::class)
                .addParameter("childId", Long::class)
                .build())
            .addFunction(FunSpec
                .constructorBuilder()
                .callThisConstructor("0L", "0L")
                .build())
            .addSuperclassConstructorParameter("parentId")
            .addSuperclassConstructorParameter("childId")
    }
}