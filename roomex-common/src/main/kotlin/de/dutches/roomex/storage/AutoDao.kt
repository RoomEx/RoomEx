@file:Suppress("unused")
package de.dutches.roomex.storage

/**
 * Annotation to use for auto generation of DAO-classes.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class AutoDao(val packageName: String = "")