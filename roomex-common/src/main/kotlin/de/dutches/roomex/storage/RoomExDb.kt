@file:Suppress("unused")
package de.dutches.roomex.storage

/**
 * Optional annotation.
 * If not set explicitly, database schema will stay on version 1
 *
 * @property version Version of the database schema.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class RoomExDb(val version: Int = 1, val exportSchema: Boolean = false)