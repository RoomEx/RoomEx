package de.dutches.roomex.processor

import androidx.room.*
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.Dependencies
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.symbol.*
import com.squareup.kotlinpoet.*
import de.dutches.roomex.processor.extensions.ksp.*
import de.dutches.roomex.processor.relation.RelationValidationProcessor
import de.dutches.roomex.storage.AutoDao
import de.dutches.roomex.storage.DaoEx

/**
 * The annotation processor which handles the [Entity] annotation.
 * Iterates over every entity class and creates new relationship entities where needed.
 * Note: Validation of the relationships and its properties is done in the [RelationValidationProcessor]
 *
 * @param codeGenerator
 * @param logger
 * @param options
 */
class AutoDaoProcessor(
    codeGenerator: CodeGenerator,
    logger: KSPLogger,
    options: Map<String, String>
) : AbstractProcessor(codeGenerator, logger, options) {

    override val annotationClass = AutoDao::class.java
    override fun getVisitor() = EntityVisitor()

    protected inner class EntityVisitor : AbstractProcessor.Visitor() {

        /**
         * Processes the given [AutoDao]-class and generated a DAO-class
         *
         * @param classDeclaration
         * @param data
         */
        override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit) {
            super.visitClassDeclaration(classDeclaration, data)

            // Retrieve the entity class for this dao
            val classShortName = classDeclaration.simpleName.getShortName() + "Dao"

            // Retrieve the target package name
            var packageName = classDeclaration.fetchAnnotationArgValue(AutoDao::class, Constants.Annotations.AutoDao.Arguments.PACKAGE_NAME) as String
            if (packageName.isBlank()) packageName = Constants.roomExPackageStorage

            builderFile = FileSpec.builder(packageName,
                classShortName)

            // Build the new dao class
            val builderClass = TypeSpec.classBuilder(classShortName)

            // Add the RoomEx @DaoEx
            builderClass.addAnnotation(AnnotationSpec.builder(DaoEx::class).build())

            // Make the class abstract
            builderClass.addModifiers(KModifier.ABSTRACT)

            // Import and set super class/interface
            builderFile.addImport(classDeclaration.packageName.asString(), classDeclaration.simpleName.getShortName())
            builderFile.addImport(Constants.roomExAutoDaoClassName, Constants.roomExAutoDaoClassName.removePrefix(Constants.roomExAutoDaoClassName))

            // Implement any method inherited by any RoomEx interface or class
            val funSpecs = implementMethods()

            // Implement any property inherited by any RoomEx interface or class
            val propSpecs = implementProperties()


            // Everything created, build the final class
            builderClass.addFunctions(funSpecs)
            builderClass.addProperties(propSpecs)

            // Build the final class
            builderFile.addType(builderClass.build())

            var classContent = builderFile.build().toString()

            // Add the RoomExDao class as super class
            // Package with the RoomExDao class cannot be added as a dependency, since it is an android package
            // Therefore we need to add it manually, post class generation
            classContent = StringBuilder(classContent)
                .insert(classContent.indexOf("class $classShortName") + "class $classShortName".length,
                    " : ${Constants.roomExAutoDaoClassName}<${classDeclaration.simpleName.getShortName()}>() ").toString()

            // Write the new dao class to a file
            addNewCodeFile(packageName,
                classShortName,
                classContent,
                Dependencies(false, *resolver.getAllFiles().toList().toTypedArray())
            )
        }


        override fun checkAndLogClassValidation(declaration: KSClassDeclaration): Boolean {
            var ret = super.checkAndLogClassValidation(declaration)

            // Class should be @Entity
            if (!declaration.isAnnotationPresent(Entity::class)) {
                logger.error("Classes annotated with the \"${AutoDao::class.simpleName}\" must also be annotated with \"${Entity::class.simpleName}\".", declaration)
                ret = false
            }

            return ret
        }
    }
}