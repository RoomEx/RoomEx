package de.dutches.roomex.sample.storage.local.dao

import de.dutches.roomex.storage.RoomExDao
import de.dutches.roomex.storage.model.RoomExEntity

abstract class IntermediateDao<T : RoomExEntity> : RoomExDao<T>() { }