package de.dutches.roomex.processor.helper

import androidx.room.Entity
import com.google.devtools.ksp.closestClassDeclaration
import com.google.devtools.ksp.getAllSuperTypes
import com.google.devtools.ksp.symbol.KSClassDeclaration
import com.google.devtools.ksp.symbol.KSType
import de.dutches.roomex.processor.Constants
import de.dutches.roomex.processor.extensions.ksp.isAnnotationPresent
import de.dutches.roomex.processor.extensions.ksp.resolveName

object DaoUtils {
    fun fetchEntityForDao(dao: KSClassDeclaration): KSType {
        // we need to figure out, which entity is being targeted by the dao
        // The easiest way: traverse hierarchy of the doa class upwards, until the RoomExDao-base class has been found
        // From that class, resolve the type argument

        val daoSuperTypes = dao.getAllSuperTypes()
        val posOfRoomExDao = daoSuperTypes.indexOfFirst { t -> t.declaration.resolveName() == Constants.roomExDaoClassName }

        // First find the RoomExDao base class
        val baseDao = daoSuperTypes.elementAt(posOfRoomExDao)

        // Fetch the entity, which is the first type argument of the base dao
        var resolved = baseDao.arguments.first().type!!.resolve()

        while (!resolved.declaration.closestClassDeclaration()!!.isAnnotationPresent(Entity::class)) {

            // At this point, resolved is base class or generic type parameter, e.g. T      -> resolved.declaration.toString()
            // We need to know its parent, e.g. IntermediateDao                             -> resolved.declaration.parent.toString()
            // With that, we can check the hierarchy for a class with that name             -> daoSuperTypes.first { t -> t.declaration.toString() == resolved.declaration.parent.toString() }
            // At this point, check the arguments again                                     -> .arguments.first().type!!.resolve()

            val parentName = resolved.declaration.parent.toString()
            val parentDao = daoSuperTypes.first { t -> t.declaration.toString() == parentName }

            // Need to check every argument
            for (arg in parentDao.arguments) {
                resolved = arg.type!!.resolve()

                if (resolved.declaration.closestClassDeclaration()!!.isAnnotationPresent(Entity::class))
                    break // Found!
            }
        }
        return resolved
    }
}