package de.dutches.roomex.sample.storage.local.dao

import androidx.room.Query
import de.dutches.roomex.sample.model.Address
import de.dutches.roomex.storage.RoomExDao
import de.dutches.roomex.storage.DaoEx
import de.dutches.roomex.storage.crud.ICrud
import de.dutches.roomex.storage.model.relation.RoomExRelationEntity

// Since this is only a sample class, suppress some warnings
@Suppress("unused")
@DaoEx
abstract class AddressDao : RoomExDao<Address>(), ICrud<Address> {

    @Query("SELECT * FROM rel_user_address_adress")
    abstract suspend fun getCurrentUserAddressRelations() : List<RoomExRelationEntity>

    @Query("SELECT * FROM rel_dummyauto_address_adress")
    abstract suspend fun getCurrentDummyAddressRelations() : List<RoomExRelationEntity>
}