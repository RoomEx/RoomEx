package de.dutches.roomex.sample.storage.local

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import de.dutches.roomex.sample.model.Address
import de.dutches.roomex.sample.model.User
import de.dutches.roomex.sample.storage.local.dao.AddressDao
import de.dutches.roomex.sample.storage.local.dao.UserDao
import de.dutches.roomex.storage.RoomExDbBase
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class DbModule {

    @Provides
    fun provideUserDaoEx(appDatabase: RoomExDbBase): UserDao {
        return appDatabase.fetchDaoForEntity(User::class) as UserDao
    }

    @Provides
    fun provideAddressDaoEx(appDatabase: RoomExDbBase): AddressDao {
        return appDatabase.fetchDaoForEntity(Address::class) as AddressDao
    }


    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): RoomExDbBase {
        return Room.databaseBuilder(
            appContext,
            RoomExDbBase.getDbClass(),
            "roomex_sample_db"
        ).build() as RoomExDbBase
    }
}