package de.dutches.roomex.storage.crud

import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Interface to add public methods to your Dao to insert, read, update and delete
 * your entities in/from the database.
 * Furthermore, additional methods are added to add or remove joins.
 *
 * @param T Entity type
 */
interface ICrud<T> : ICreate<T>, IRead<T>, IUpdate<T>, IDelete<T>, IJoins<T> where T : RoomExEntity