import org.jetbrains.dokka.gradle.DokkaTask
import java.net.URI
import java.util.*

plugins {
    kotlin("jvm")
    `java-library`
    `maven-publish`
    signing
    id("org.jetbrains.dokka") version "1.7.20"
    id("idea")
}

group = getSetting("roomExGroupName")
version = "${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}"

repositories {
    google()
    mavenCentral()
}

dependencies {
    implementation("androidx.room:room-common:${getSetting("roomVersion")}")
}

tasks.jar {
    manifest {
        attributes(mapOf(
            "Implementation-Title" to project.name,
            "Implementation-Version" to project.version))
    }
}

val isReleaseVersion = !getSetting("roomExVersion").endsWith("SNAPSHOT")
val artifact = "$buildDir/libs/${project.name}-${project.version}.jar"

val dokkaHtml by tasks.getting(DokkaTask::class)
val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    from(kotlin.sourceSets.main.get().kotlin)
}

val javadocJar by tasks.creating(Jar::class) {
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    description = "Assembles Javadoc JAR"
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    archiveClassifier.set("javadoc")
    from(dokkaHtml)
}

publishing {

    repositories {
        maven {
            val releaseRepo = URI("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
            val snapshotRepo = URI("https://s01.oss.sonatype.org/content/repositories/snapshots/")
            name = "OSSRH"
            url = if (isReleaseVersion) releaseRepo else snapshotRepo

            credentials {
                username = getSetting("ossrh_user")
                password = getSetting("ossrh_pass")
            }
        }
    }


    publications {
        create<MavenPublication>("maven") {
            // Add sources and documentation
            artifact(javadocJar)
            artifact(sourcesJar)
            //artifact(artifact)

            from(components["java"])

            // Build the POM
            pom {
                groupId = getSetting("roomExGroupName")
                version = "${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}"

                name.set(project.name)
                inceptionYear.set("2022")
                url.set(getSetting("pom_url"))
                description.set("Common package for RoomEx, a relationship manager for Android Room.")
                packaging = "jar"

                licenses {
                    license {
                        name.set("MIT")
                        url.set("https://opensource.org/licenses/MIT")
                    }
                }

                developers {
                    developer {
                        id.set(getSetting("pom_dev_id"))
                        name.set(getSetting("pom_dev_name"))
                        email.set(getSetting("pom_dev_mail"))
                        url.set(getSetting("pom_dev_url"))
                    }
                }

                issueManagement {
                    system.set(getSetting("pom_issue_system"))
                    url.set(getSetting("pom_issue_url"))
                }

                scm {
                    connection.set(getSetting("pom_scm_con"))
                    developerConnection.set(getSetting("pom_scm_dev_con"))
                    url.set(getSetting("pom_scm_dev_con"))
                }
                dependencies {

                }
            }
        }
    }
}

tasks.withType<Sign> {
    onlyIf { isReleaseVersion }
}

signing {
    useGpgCmd()
    sign(publishing.publications)
}

fun getSetting(key: String) = project.getSetting(key)
fun Project.getSetting(key: String): String {

    // Check if local properties
    val localProperty = rootProject.file("local.properties")
    if (localProperty.isFile) {
        val props = Properties().apply { load(localProperty.inputStream()) }
        //props.load(localProperty.inputStream())
        if (props.containsKey(key))
            return props[key].toString()
    }

    // Not in local, check gradle properties
    if (!project.properties.containsKey(key))
        throw Exception("Key $key not found in gradle.properties")
    return project.properties[key].toString()
}