package de.dutches.roomex.processor.extensions.kotlinpoet

import com.squareup.kotlinpoet.CodeBlock
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.joinToCode

internal fun TypeName.createConstructorCall(vararg args: String) : CodeBlock {
    val argsCode = args
        .map { CodeBlock.of("%L", it) }
        .joinToCode(separator = ", ", prefix = "(", suffix = ")")
    return CodeBlock.Builder()
        .add("%T", this)
        .add(argsCode)
        .build()

}
