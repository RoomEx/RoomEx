package de.dutches.roomex.processor.extensions.ksp

import com.google.devtools.ksp.symbol.KSAnnotation

/**
 * Retrieve the argument value of an annotation.
 *
 * @param argument The argument of which the value needs to be fetched
 * @return The found value. String and char values are returned escaped.
 */
internal fun KSAnnotation.fetchArgValue(argument: String): Any? {
    return arguments.firstOrNull() { arg -> arg.name?.asString() == argument }
        ?.value
}

/**
 * Resolves the name for an annotation, either its simple, or its qualified name.
 *
 * @return The found name as string.
 */
internal fun KSAnnotation.resolveName(simpleName: Boolean = false) : String {
    return  if (simpleName)
                this.shortName.getShortName()
            else
                this.annotationType.resolve().declaration.resolveName()
}