package de.dutches.roomex.sample.model

import androidx.room.Entity
import androidx.room.Ignore
import de.dutches.roomex.storage.model.relation.Cascade
import de.dutches.roomex.storage.model.relation.OneToOneEx

// Since this is only a sample class, suppress some warnings
@Suppress("unused")
@Entity(tableName = "contact")
class Contact(
    id: Long,

    var phone : String,

    var email : String


) : SecondIntermediateModel<Contact>(id) {

    @Ignore
    @OneToOneEx(mappedWith = "contact", cascade = Cascade.All)
    var address : IAddress? = null

    constructor(phone: String, email: String) : this(0L, phone, email)
}