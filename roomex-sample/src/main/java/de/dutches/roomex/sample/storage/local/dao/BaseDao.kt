package de.dutches.roomex.sample.storage.local.dao

import de.dutches.roomex.storage.RoomExDao
import de.dutches.roomex.storage.model.RoomExEntity

@Suppress("unused")
abstract class BaseDao<T : RoomExEntity> : RoomExDao<T>() { }