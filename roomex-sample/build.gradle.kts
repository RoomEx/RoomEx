import java.util.*

val roomVersion: String by project
val kotlinVersion: String by project
val kspVersion: String by project
val roomExVersion: String by project

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("com.google.devtools.ksp")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
    id("idea")
}


idea {
    module {
        testSourceDirs.add(file("build/generated/ksp"))
        generatedSourceDirs.add(file("build/generated/ksp"))
    }
}

android {
    compileSdk = 33
    namespace = "de.dutches.roomex.sample"

    @Suppress("UnstableApiUsage")
    defaultConfig {
        applicationId = "de.dutches.roomex.sample"
        minSdk = 19
        targetSdk = 33
        versionCode = 1
        versionName = "$kotlinVersion-$roomExVersion"
        multiDexEnabled = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        javaCompileOptions {
            annotationProcessorOptions {

                /**
                 * Uncomment the following line to create a database schema file
                 * With that you can easily check if your entities are created correctly
                 */
                //arguments["room.schemaLocation":"$projectDir/schemas".toString()]
                //arguments["room.incremental"] = "true"
            }
        }
    }

    @Suppress("UnstableApiUsage")
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
    }

    @Suppress("UnstableApiUsage")
    buildFeatures {
        viewBinding = true
    }


    kotlin.sourceSets {
        main {
            java {
                kotlin.srcDir(file("build/generated/ksp/src/main"))
            }
        }
        test {
            java {
                kotlin.srcDir(file("build/generated/ksp/src/test"))
            }
        }
    }
}

kotlin.sourceSets {
    main {
        java {
            kotlin.srcDir(file("build/generated/ksp/src/main"))
        }
    }
    test {
        java {
            kotlin.srcDir(file("build/generated/ksp/src/test"))
        }
    }
}

repositories {
    google()
    //mavenLocal()
    mavenCentral()
}

dependencies {

    // Default Android packages
    implementation("androidx.appcompat:appcompat:1.6.0")
    implementation("com.google.android.material:material:1.7.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.navigation:navigation-fragment-ktx:2.6.0-alpha04")
    implementation("androidx.navigation:navigation-ui-ktx:2.6.0-alpha04")

    // Android Room
    implementation("androidx.room:room-ktx:$roomVersion")
    kapt("androidx.room:room-compiler:$roomVersion")
    implementation("androidx.room:room-runtime:$roomVersion")


    // RoomEx
    //implementation(project(mapOf("path" to ":roomex-android")))
    //ksp(project(path = ":roomex-processor", configuration = "default"))
    implementation("de.dutches.roomex:roomex-android:${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}")
    implementation("de.dutches.roomex:roomex-common:${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}")
    ksp("de.dutches.roomex:roomex-processor:${getSetting("kotlinVersion")}-${getSetting("roomExVersion")}")


    // Testing
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.4")


    // Hilt dependencies
    implementation("com.google.dagger:hilt-android:2.40.5")
    kapt("com.google.dagger:hilt-android-compiler:2.40.5")
}


fun getSetting(key: String) = project.getSetting(key)
fun Project.getSetting(key: String): String {

    // Check if local properties
    val localProperty = rootProject.file("local.properties")
    if (localProperty.isFile) {
        val props = Properties().apply { load(localProperty.inputStream())}
        if (props.containsKey(key))
            return props[key].toString()
    }

    // Not in local, check gradle properties
    if (!project.properties.containsKey(key))
        throw Exception("Key $key not found in gradle.properties")
    return project.properties[key].toString()
}