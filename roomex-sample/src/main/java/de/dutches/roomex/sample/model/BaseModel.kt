package de.dutches.roomex.sample.model

import de.dutches.roomex.storage.model.RoomExEntity

abstract class BaseModel(id: Long) : RoomExEntity(id) {
}