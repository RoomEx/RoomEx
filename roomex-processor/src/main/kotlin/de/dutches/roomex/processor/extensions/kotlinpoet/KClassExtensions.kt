package de.dutches.roomex.processor.extensions.kotlinpoet

import com.google.devtools.ksp.symbol.KSDeclaration
import com.squareup.kotlinpoet.ClassName
import kotlin.reflect.KClass

/**
 * Builds the ClassName which corresponds to the given declaration.
 *
 * @return The instance of ClassName
 */
internal fun KSDeclaration.buildClassName() : ClassName {
    val decl = this
    return ClassName(
        decl.packageName.asString(),
        decl.qualifiedName!!.asString().removePrefix("${decl.packageName.asString()}.").split(".")
    )
}

/**
 * Builds the ClassName which corresponds to the given class.
 *
 * @param T The type argument
 * @return The instance of ClassName
 */
internal fun <T : Any> KClass<T>.buildClassName() : ClassName {
    val pkgName = this.qualifiedName?.removeSuffix(".${this.simpleName}") ?: ""

    return ClassName(
        pkgName,
        this.qualifiedName!!.removePrefix("${pkgName}.").split(".")
    )
}