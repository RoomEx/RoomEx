package de.dutches.roomex.sample.dao

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import de.dutches.roomex.sample.model.Address
import de.dutches.roomex.sample.model.User
import de.dutches.roomex.sample.storage.local.dao.AddressDao
import de.dutches.roomex.sample.storage.local.dao.UserDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class UserDaoTest : BaseDaoTest() {

    private lateinit var userDao: UserDao
    private lateinit var addressDao: AddressDao

    @Before
    fun setup() {
        userDao = db.fetchDaoForEntity(User::class) as UserDao
        addressDao = db.fetchDaoForEntity(Address::class) as AddressDao
    }

    @After
    fun tearDown() {
        db.close()
    }

    @Test
    fun insertNewUser() = runTest {
        // Given
        val user = User(name = "John",surname = "Doe")

        // When
        val id = userDao.insert(user)

        // Then
        assertNotEquals(id, 0L)
    }

    @Test
    fun insertAndGetNewUser() = runTest {

        // Given
        val user = User(name = UUID.randomUUID().toString(),surname = "Doe")

        // When
        userDao.insert(user)

        // Then
        assertTrue(userDao.getAll().any { u -> u.name == user.name })
    }

    @Test
    fun insertNewUserAndCheckIfExists() = runTest {

        // Given
        val user = User(name = UUID.randomUUID().toString(),surname = "Doe")

        // When
        val id = userDao.insert(user)

        // Then
        assertTrue(userDao.exists(id))
    }

    @Test
    fun checkExistsOfWrongUserShouldFail() = runTest {

        // Given
        val maxId = userDao.getAll().maxByOrNull { u -> u.id }?.id ?: 0

        // Then
        assertFalse(userDao.exists(maxId + 1 ))
    }

    @Test
    fun shouldDeleteNewUserById() = runTest {

        // Given
        val user = User(name = UUID.randomUUID().toString(),surname = "Doe")

        // When
        val id = userDao.insert(user)
        userDao.delete(id)

        // Then
        assertFalse(userDao.exists(id))
    }

    @Test
    fun insertNewUserWithNewAddress() = runTest {

        // Given
        val user = User(name = "John",surname = "Doe")
        user.address = Address(UUID.randomUUID().toString(), "Hamburg")

        // When
        userDao.insertWithChildren(user)

        // Then
        assertTrue(addressDao.getAll().any { u -> u.streetname == user.address!!.streetname})
    }

    @Test
    fun fetchUserAddress() = runTest {

        // Given
        val user = User(name = "John",surname = "Doe")
        user.address = Address(UUID.randomUUID().toString(), "Hamburg")
        userDao.insertWithChildren(user)

        // When
        userDao.loadChildren(user, fields = arrayOf("address"))

        assertNotNull(user.address)
        assertNotEquals(user.address!!.id, 0L)
    }

    @Test
    fun shouldAddNewFriendWithAddJoinMethod() = runTest {

        // Given
        var user: User? = User(name = "John",surname = "Doe")
        val friend1 = User("Johns Friend #1", "#1")
        val friend2 = User("Johns Friend #2", "#2")

        user!!.id = userDao.insert(user)
        friend1.id = userDao.insert(friend1)

        // When
        userDao.addJoin(user, friend1, "friends")
        userDao.addJoin(user, friend2, "friends")
        user = userDao.getByIdWithChildren(user.id, fields = arrayOf("friends"))

        // Then
        assertNotNull(user)
        assertNotNull(user!!.friends)
        assertEquals(user.friends!!.size, 2)
    }

    @Test
    fun shouldAddNewFriendsWithAddJoinsMethod() = runTest {

        // Given
        var user: User? = User(name = "John",surname = "Doe")
        val friends = listOf(
            User("Johns Friend #1", "#1"),
            User("Johns Friend #2", "#2"))

        user!!.id = userDao.insert(user)

        // When
        userDao.addJoins(user, friends, "friends")
        user = userDao.getByIdWithChildren(user.id, fields = arrayOf("friends"))

        // Then
        assertNotNull(user)
        assertNotNull(user!!.friends)
        assertEquals(user.friends!!.size, 2)
        assertNotEquals(user.friends!!.first().id, 0L)
    }

    @Test
    fun shouldRemoveFriendWithRemoveJoinMethod() = runTest {

        // Given
        var user: User? = User(name = "John",surname = "Doe")
        val friend1 = User("Johns Friend #1", "#1")
        val friend2 = User("Johns Friend #2", "#2")

        user!!.id = userDao.insert(user)
        friend1.id = userDao.insert(friend1)

        // When
        userDao.addJoin(user, friend1, "friends")
        userDao.addJoin(user, friend2, "friends")
        userDao.removeJoin(user, friend2, "friends")
        user = userDao.getByIdWithChildren(user.id, fields = arrayOf("friends"))

        // Then
        assertNotNull(user)
        assertNotNull(user!!.friends)
        assertEquals(user.friends!!.size, 1)
    }

    @Test
    fun shouldRemoveFriendsWithRemoveJoinsMethod() = runTest {

        // Given
        var user: User? = User(name = "John",surname = "Doe")
        val friend0 = User("Johns Friend #0", "#0")
        val friends = listOf(
            User("Johns Friend #1", "#1"),
            User("Johns Friend #2", "#2"))

        user!!.id = userDao.insert(user)

        // When
        friend0.id = userDao.insert(friend0)
        userDao.addJoin(user, friend0, "friends")
        userDao.addJoins(user, friends, "friends")
        userDao.removeJoins(user, friends, "friends")
        user = userDao.getByIdWithChildren(user.id, fields = arrayOf("friends"))

        // Then
        assertNotNull(user)
        assertNotNull(user!!.friends)
        assertEquals(user.friends!!.size, 1)
        assertEquals(user.friends!!.first().id, friend0.id)
    }

    @Test
    fun shouldOnlyFetchFriends() = runTest {

        // Given
        var user: User? = User(name = "John",surname = "Doe")
        user!!.address = Address(UUID.randomUUID().toString(), "Hamburg")
        user.friends = mutableListOf(User("F1", "Friend"), User("F2", "Friend"))
        userDao.insertWithChildren(user)

        // When
        user = userDao.getByIdWithChildren(user.id, fields = arrayOf("friends"))

        // Then
        assertNotNull(user)
        assertNotNull(user!!.friends)
        assertNull(user.address)
    }

    @Test
    fun removeUserAddressJoin() = runTest {

        // Given
        var user = User(name = "John",surname = "Doe")
        user.address = Address(UUID.randomUUID().toString(), "Hamburg")
        userDao.insertWithChildren(user)

        // When
        user.address = null
        userDao.updateWithChildren(user, removeIfNull = true)
        user = userDao.getByIdWithChildren(user.id)!!

        assertNull(user.address)
    }

    @Test
    fun removedAddressJoinShouldKeepEntity() = runTest {

        // Given - new User with Address
        val user = User(name = "John",surname = "Doe")
        user.address = Address(UUID.randomUUID().toString(), "Hamburg")
        user.id = userDao.insertWithChildren(user)
        userDao.insertWithChildren(user)
        val addressId = user.address!!.id

        // When - Address join removed from user
        user.address = null
        userDao.updateWithChildren(user, removeIfNull = true)
        userDao.loadChildren(user)
        val address = addressDao.getById(addressId)

        // Then - Relation should be gone, but address entity should exists
        assertNull(user.address)
        assertNotNull(address)
        assertNotEquals(address!!.id, 0L)
    }

    @Test
    fun updateJoinsWithNonExistingFieldShouldThrowException() = runTest {

        // Given
        val user = User(name = "John",surname = "Doe")
        userDao.insert(user)

        // When // Then
        assertThrows(IllegalArgumentException::class.java) {
            runBlocking { userDao.updateWithChildren(user, fields = arrayOf("NonExisingField")) }
        }
    }

    @Test
    fun fetchChildrenWithNonExistingFieldShouldThrowException() = runTest {
        // Given
        val user = User(name = "John",surname = "Doe")
        user.id = userDao.insert(user)

        // When // Then
        assertThrows(IllegalArgumentException::class.java) {
            runBlocking { userDao.getByIdWithChildren(user.id, fields = arrayOf("NonExisingField")) }
        }
    }

    @Test
    fun shouldThrowExceptionWhenDeleteCascadeNotSet() = runTest {
        // Given
        val user = User(name = "John",surname = "Doe")
        user.id = userDao.insertWithChildren(user)


        // When // Then
        assertThrows(IllegalArgumentException::class.java) {
            runBlocking { userDao.deleteChildren(user, fields = arrayOf("dad")) }
        }
    }

    @Test
    fun shouldDeleteCascadeChildren() = runTest {
        // Given
        val user = User(name = "John",surname = "Doe")
        var child0: User? = User("Johns child #0", "#0")
        val child1 = User("Johns child #1", "#1")
        val child2 = User("Johns child #2", "#2")

        var child0OfChild0 : User? = User("Child of Johns child #0", "#0")
        child0!!.children = mutableListOf(child0OfChild0!!)
        user.children = mutableListOf(child0, child1, child2)


        child0OfChild0.id = userDao.insertWithChildren(child0OfChild0)
        child0.id = userDao.insertWithChildren(child0)
        user.id = userDao.insertWithChildren(user)


        // When
        userDao.deleteChildren(user, fields = arrayOf("children"), clearFromEntity = false)
        child0 = userDao.getById(child0.id)
        child0OfChild0 = userDao.getById(child0OfChild0.id)

        // Then
        assertNotNull(user.children)
        assertNull(child0)
        assertNotNull(child0OfChild0) // Should not be deleted without recursively = true
    }

    @Test
    fun shouldDeleteCascadeChildrenReflect() = runTest {
        // Given
        val user = User(name = "John",surname = "Doe")
        var child0: User? = User("Johns child #0", "#0")
        val child1 = User("Johns child #1", "#1")
        val child2 = User("Johns child #2", "#2")

        var child0OfChild0 : User? = User("Child of Johns child #0", "#0")
        child0!!.children = mutableListOf(child0OfChild0!!)
        user.children = mutableListOf(child0, child1, child2)


        child0OfChild0.id = userDao.insertWithChildren(child0OfChild0)
        child0.id = userDao.insertWithChildren(child0)
        user.id = userDao.insertWithChildren(user)


        // When
        userDao.deleteChildren(user, fields = arrayOf("children"), clearFromEntity = true)
        child0 = userDao.getById(child0.id)
        child0OfChild0 = userDao.getById(child0OfChild0.id)

        // Then
        assertNull(user.children)
        assertNull(child0)
        assertNotNull(child0OfChild0) // Should not be null without recursively = true
    }

    @Test
    fun recursiveLoadingShouldNotEndInInfiniteLoop() = runTest {
        // Given
        var user: User? = User(name = "John",surname = "Doe")
        user!!.dad = User("Johns Dad #0", "#0")

        user.id = userDao.insertWithChildren(user)
        user = userDao.getByIdWithChildren(user.id)

        // When
        val dad = user!!.dad
        dad!!.dad = user
        userDao.updateWithChildren(dad)
        user = userDao.getByIdWithChildren(user.id, fields = arrayOf("dad"), recursively = true)

        // Then
        assertNotNull(user)
        assertNotNull(user!!.dad)
        assertNotNull(user.dad!!.dad)
        assertEquals(user.id, user.dad!!.dad!!.id)
    }
}
