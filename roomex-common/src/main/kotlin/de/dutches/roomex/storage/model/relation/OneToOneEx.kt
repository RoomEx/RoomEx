@file:Suppress("unused")
package de.dutches.roomex.storage.model.relation

import de.dutches.roomex.storage.model.RoomExEntity
import kotlin.reflect.KClass

@Target(AnnotationTarget.PROPERTY, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.SOURCE)
annotation class OneToOneEx(val mappedWith : String = "",
                            val cascade : Cascade = Cascade.None,
                            val entityKClass : KClass<out RoomExEntity> = RoomExEntity::class,
                            val name : String = "")