# Contributing
RoomEx has been created as a bachelor's thesis. As of such, it might be missing some useful functions.   
Of course, as with every piece of software, RoomEx most definitly will also have some bugs hidden somewhere.   

Any help with resolving bugs or implementing new features is welcome and greatly appreciated.    
Bug reporting, or feature requests, is also a great way to contribute to this project.    
When opening an issue or pull requests, please follow the remarks on this page..

## Issues

Issues are very valuable to this project.

  - Ideas are a valuable source of contributions others can make
  - Problems show where this project is lacking
  - With a question you show where contributors can improve the user experience

Thank you for creating them.

## Pull Requests

Pull requests are a great way to get your ideas into this repository.

When deciding if we merge in a pull request, we look at the following things:

### Does it state intent

You should be clear which problem you're trying to solve with your contribution.

For example:

> Add link to code of conduct in README.md

Doesn't tell me anything about why you're doing that

> Add link to code of conduct in README.md because users don't always look in the CONTRIBUTING.md

Tells us the problem that you have found, and the pull request shows us the action you have taken to solve it.

### Is it of good quality
Not only should your code compile without failures and warnings, but it should also:

  - include new tests for new features.
  - be readable and easy to understand.
  - include comments where necessary.


### Should it be part of RoomEx
New features are always great, but they should always tackle any problem with Room, which RoomEx tries to solve.

For example, a PR which introduces a nice way to log messages to a file should not be part of RoomEx.

### Does it follow the contributor covenant

This repository has a [code of conduct](Contributor-Code-of-Conduct), we won't merge pull requests that do not respect it.

