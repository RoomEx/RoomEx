package de.dutches.roomex.sample.model

import de.dutches.roomex.storage.model.RoomExEntity

abstract class SecondIntermediateModel<T : RoomExEntity>(id: Long) : IntermediateModel<T>(id) {
}