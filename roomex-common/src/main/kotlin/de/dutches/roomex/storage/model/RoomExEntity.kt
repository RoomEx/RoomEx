package de.dutches.roomex.storage.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

/**
 * Base class of entities to use with RoomEx.
 *
 * @property id The record identifier.
 */
abstract class RoomExEntity(

    /**
     * Record identifier
     */
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
) {
}