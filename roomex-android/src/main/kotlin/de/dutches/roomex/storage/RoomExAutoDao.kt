package de.dutches.roomex.storage

import de.dutches.roomex.storage.crud.*
import de.dutches.roomex.storage.model.RoomExEntity

/**
 * Base class for Dao's generated with the [AutoDao] annotation.
 *
 * @param T The entity txpe
 */
abstract class RoomExAutoDao<T> : RoomExDao<T>(), ICrud<T> where T : RoomExEntity