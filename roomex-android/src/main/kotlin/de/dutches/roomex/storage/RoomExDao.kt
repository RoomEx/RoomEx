@file:Suppress("unused")
package de.dutches.roomex.storage

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import de.dutches.roomex.extensions.emptyToNull
import de.dutches.roomex.storage.model.RoomExEntity
import de.dutches.roomex.storage.model.relation.Cascade
import de.dutches.roomex.storage.model.relation.RoomExRelationEntity
import kotlin.reflect.KClass

/**
 * Base class for DAOs to use with RoomEx.
 * Contains methods to easily create and remove joins and to fetch the corresponding child data.
 *
 * @param T Entity to use with this DAO.
 */
abstract class RoomExDao<T> where T : RoomExEntity  {

    /** Map with all relation fields */
    protected abstract val relationFields : Map<String, EntityRelPropData>

    //region CREATE

    /**
     * Insert a new entity in the database.
     *
     * @param entity The entity to store in the database.
     * @return New id of the entity.
     */
    @Insert
    protected abstract suspend fun insertEntity(entity: T): Long

    /**
     * Insert a new entity and its children in the database.
     *
     * @param entity The entity to store in the database.
     * @param recursively If children of the children.
     * @return New id of the parent entity.
     */
    protected open suspend fun insertEntityWithChildren(entity: T, recursively: Boolean = false): Long {
        return insertEntityWithChildrenImpl(entity = entity, recursively = recursively)
    }

    /**
     * Insert a new entity and its children in the database.
     * Note: If a child already has an id,
     *
     * @param entity The entity to store in the database.
     * @param recursively Should children be fetched recursively.
     * @param fields The field name of any child fields to process.
     * @return New id of the parent entity.
     */
    protected open suspend fun insertEntityWithChildren(entity: T, recursively: Boolean = false, vararg fields: String): Long {
        return insertEntityWithChildrenImpl(entity = entity, recursively = recursively, fields = fields)
    }

    private suspend fun insertEntityWithChildrenImpl(entity: T,
                                                     recursively: Boolean,
                                                     vararg fields : String): Long {
        if (entity.id == 0L) {
            entity.id = insertEntity(entity)

            if (entity.id == 0L)
                throw Exception("Entity could not be saved to the database.")
        }

        val fieldsToProcess =   fields.asList().filterNot { f -> f.isEmpty() }.emptyToNull()
                                ?: relationFields.filter { r -> r.value.cascade.hasFlag(Cascade.Insert) }.keys

        // loop over all given fields and insert and join children
        for (field in fieldsToProcess)  {

            // skip invalid fields
            if (!relationFields.containsKey(field))
                throw IllegalArgumentException("Field $field is not a valid relation field for the current entity ${entity::class.simpleName}")
            if (!relationFields[field]!!.cascade.hasFlag(Cascade.Insert))
                throw IllegalArgumentException("Field $field is not marked with 'insertCascade'")

            // Check whether to reverse id fields
            val isMappedField = relationFields[field]!!.mappedWith.isNotEmpty()

            // List with instanced entity values for this field
            // Must be nullable because we only process fields which are set
            var values: MutableList<RoomExEntity>? = null

            // Read instance values
            if (relationFields[field]?.type == List::class)
                values = this.asTypedIRoomInternalExDao().readPropertyList(entity, field) as MutableList<RoomExEntity>?
            else if (RoomExEntity::class.java.isAssignableFrom(relationFields[field]!!.childEntityClass.java)) {
                val value = this.asTypedIRoomInternalExDao().readPropertySingle(entity, field)
                if (value != null)
                    values = mutableListOf(value)
            }


            // Continue with next if no child has been set
            if (values == null)
                continue

            // Check if any new entity needs to be inserted first
            val childDao = fetchDaoForEntity(relationFields[field]!!.childEntityClass)

            // Insert all new entities for this field
            for (it in values.filter { v -> v.id == 0L }) {
                it.id = childDao.asTypedRoomExDao().asTypedIRoomInternalExDao().rinsert(it)
            }

            // Next step, insert the relation in the relation table
            for (it in values) {
                join(entity.id, it.id, relationFields[field]!!.relationTableName, isMappedField)
            }

            if (recursively) {
                for (child in values) {
                    childDao.asTypedRoomExDao()
                        .insertEntityWithChildrenImpl(child, true)
                }
            }
        }
        return entity.id
    }

    //endregion CREATE

    //region READ

    /**
     * Check whether an entity with the given id exists.
     *
     * @param id Id of the entity to check for
     * @return True, if entity exists
     */
    protected open suspend fun existsEntity(id: Long): Boolean {
        @Suppress("unchecked_cast")
        return (this as IRoomExInternalDao<T>).rexists(id)
    }

    /**
     * Get the entity with the given id.
     *
     * @param id Primary key of the entity.
     * @return The found entity or null.
     */
    protected open suspend fun getEntityById(id: Long): T? {

        @Suppress("unchecked_cast")
        return (this as IRoomExInternalDao<T>).rread(id)
    }

    /**
     * Get all entities from the database.
     *
     * @return All entities in the database.
     */
    protected open suspend fun getAllEntities(): MutableList<T> {
        @Suppress("unchecked_cast")
        return (this as IRoomExInternalDao<T>).rreadAll()
    }

    /**
     * Get the entity with the given id including its children.
     *
     * @param id Primary key of the entity.
     * @param recursively Should children be fetched recursively.
     * @param fields The field name of any child fields to load.
     * @return The found entity with its children or null.
     */
    protected open suspend fun getEntityByIdWithChildren(id: Long, recursively: Boolean = false, vararg fields: String) : T? {
        val entity = getEntityById(id = id)

        if (entity != null) {
            loadEntityChildrenImpl(
                entity = entity,
                recursively = recursively,
                fields = fields)
        }

        return entity
    }

    /**
     * Load children for the given entity.
     * The children are directly added to the entity, overwriting any existing value.
     *
     * @param entity The entity for which the children should be loaded.
     * @param recursively Should children be fetched recursively.
     * @return The entity with its children.
     */
    protected open suspend fun loadEntityChildren(entity: T, recursively: Boolean = false) : T {
        return loadEntityChildrenImpl(
            entity = entity,
            recursively = recursively)
    }

    /**
     * Load children for the given entity.
     * The children are directly added to the entity, overwriting any existing value.
     *
     * @param entity The entity for which the children should be loaded.
     * @param recursively Should children be fetched recursively.
     * @param fields The field names of the children to load.
     * @return The entity with its children.
     */
    protected open suspend fun loadEntityChildren(entity: T, recursively: Boolean = false, vararg fields : String) : T {
        return loadEntityChildrenImpl(
            entity = entity,
            recursively = recursively,
            fields = fields)
    }

    private suspend fun loadEntityChildrenImpl(entity: T,
                                         vararg fields : String,
                                         recursively: Boolean,
                                         entityCache: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>> = mutableMapOf()) : T {
        val resultMap = loadChildrenToMapImpl(
            entity = entity,
            recursively = recursively,
            entityCache = entityCache,
            fields = fields)

        for (result in resultMap) {
            if (relationFields[result.key]?.type == List::class) {
                @Suppress("unchecked_cast")
                this.asTypedIRoomInternalExDao().writePropertyList(
                    entity = entity,
                    name = result.key,
                    value = result.value as MutableList<RoomExEntity>?)
            }
            else {
                this.asTypedIRoomInternalExDao().writePropertySingle(
                    entity = entity,
                    name = result.key,
                    value = result.value as RoomExEntity?)
            }
        }

        return entity
    }

    /**
     * Load children for the given entity.
     *
     * @param entity The entity for which the children should be loaded.
     * @param recursively Should children be fetched recursively.
     * @param fields The field names of the children to load.
     * @return A map with all child data.
     */
    protected open suspend fun loadEntityChildrenToMap(entity: T, recursively: Boolean, vararg fields : String) : Map<String, Any?> {
        return loadChildrenToMapImpl(
            entity = entity,
            recursively = recursively,
            fields = fields)
    }

    private suspend fun loadChildrenToMapImpl(entity: T,
                                              recursively: Boolean,
                                              vararg fields : String,
                                              entityCache: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>> = mutableMapOf()) : Map<String, Any?> {
        if (entity.id <= 0)
            throw IllegalArgumentException("Entity has no id")

        val resultMap = mutableMapOf<String, Any?>()

        if (recursively)
            addToEntityCache(entityCache, entity, entity::class)

        val fieldsToRead =  fields.asList().filterNot { f -> f.isEmpty() }.emptyToNull()
                            ?: relationFields.filter { r -> r.value.cascade.hasFlag(Cascade.Read) }.keys

        // loop over all given fields and fetch the childdata
        for (field in fieldsToRead) {

            // Notify about invalid fields
            if (!relationFields.containsKey(field))
                throw IllegalArgumentException("Field $field is not a valid relation field for the current entity ${entity::class.simpleName}")
            if (!relationFields[field]!!.cascade.hasFlag(Cascade.Read))
                throw IllegalArgumentException("Field $field is not marked with 'readCascade'")

            // Check whether to reverse id fields
            val isMappedField = relationFields[field]!!.mappedWith.isNotEmpty()

            // Get an instance of the child dao
            val childDao = fetchDaoForEntity(entityKClass = relationFields[field]!!.childEntityClass)

            // Fetch children
            // If recursively, children of children will also be fetched
            val children: MutableList<RoomExEntity>
            children = loadChildren(
                childDao = childDao,
                entity = entity,
                fieldName = field,
                isMappedField = isMappedField,
                recursively = recursively,
                entityCache = entityCache)

            resultMap[field] =  if (relationFields[field]?.type == List::class) children
                                else children.firstOrNull()
        }
        return resultMap
    }

    private suspend fun loadChildren(
        childDao: RoomExDao<out RoomExEntity>,
        entity: T, fieldName: String,
        isMappedField: Boolean,
        recursively: Boolean,
        currentJoins: MutableList<RoomExRelationEntity>? = null,
        entityCache: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>> = mutableMapOf()) : MutableList<RoomExEntity> {
        if (recursively) {
            val result: MutableList<RoomExEntity>
            val curJoins = currentJoins?.toMutableList() ?: fetchJoins(entity.id, relationFields[fieldName]!!.relationTableName, isMappedField).toMutableList()
            val ids = mutableListOf<Long>()
            val cachedEntities = mutableListOf<RoomExEntity>()

            for (j in curJoins) {
                if (!isInEntityCache(entityCache, j.childId, relationFields[fieldName]!!.childEntityClass)) {
                    ids += j.childId // Not cached, so load von DB
                }
                else { // Cached, so simply get
                    cachedEntities += getFromEntityCache(
                        entityCache,
                        j.childId,
                        relationFields[fieldName]!!.childEntityClass
                    )!!
                }
            }

            result = childDao.asTypedRoomExDao().fetchAllChildren(
                parentId = entity.id,
                relTable = relationFields[fieldName]!!.relationTableName,
                childTable = relationFields[fieldName]!!.childEntityTableName,
                isMappedField = isMappedField,
                ids = ids)

            // Add the newly found children to the cache
            // Must be before loading its children recursively, to prevent infinite loop
            addToEntityCache(entityCache, result, relationFields[fieldName]!!.childEntityClass)
            for (r in result) {
                childDao.asTypedRoomExDao().loadEntityChildrenImpl(
                    entity = r,
                    recursively = true,
                    entityCache = entityCache)
            }

            result += cachedEntities
            result.sortBy { r -> r.id }
            return result
        }
        else { // We are working recursively, so no endless loop will occur, therefore, simply get all children without fetching the ids, also no cache is needed
            return childDao.asTypedRoomExDao().fetchAllChildren(entity.id, relationFields[fieldName]!!.relationTableName, relationFields[fieldName]!!.childEntityTableName, isMappedField)
        }
    }

    private fun addToEntityCache(entityCache: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>>,
                                 child: RoomExEntity,
                                 childKlass: KClass<out RoomExEntity>) {
        if (!entityCache.containsKey(childKlass))
            entityCache[childKlass] = mutableMapOf()

        entityCache[childKlass]!![child.id] = child
    }

    private fun addToEntityCache(entityCache: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>>,
                                 children: MutableList<RoomExEntity>,
                                 childKlass: KClass<out RoomExEntity>) {
        if (!entityCache.containsKey(childKlass))
            entityCache[childKlass] = mutableMapOf()

        entityCache[childKlass]!! += children.map { Pair(it.id, it) }
    }

    private fun getFromEntityCache(entityCache: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>>,
                                   child: Long,
                                   childKlass: KClass<out RoomExEntity>): RoomExEntity? {

        return entityCache[childKlass]?.get(child)
    }

    private fun isInEntityCache(entityCache: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>>,
                                child: Long,
                                childKlass: KClass<out RoomExEntity>): Boolean {

        return entityCache.containsKey(childKlass)
            && entityCache[childKlass]!!.containsKey(child)
    }

    //endregion READ

    //region UPDATE

    /**
     * Update the given entity in the database.
     *
     * @param entity The entity to update.
     */
    @Update
    protected abstract suspend fun updateEntity(entity: T)

    /**
     * Updates the given entity and its relations in the database.
     *
     * @param entity The entity to update in the database.
     * @param removeIfNull Determines if relation fields which are set to null should be processed or not.
     * @param removeFromList Determines if list relation fields should be processed for disjoining.
     * @param recursively Should children be updated recursively.
     */
    protected open suspend fun updateEntityWithChildren(entity: T, removeIfNull: Boolean = false, removeFromList: Boolean = true,
                                                  recursively: Boolean = false) {
        return updateEntityWithChildrenImpl(
            entity = entity,
            removeIfNull = removeIfNull,
            removeFromList = removeFromList,
            recursively = recursively)
    }

    /**
     * Updates the given entity and its relations in the database.
     *
     * @param entity The entity to update in the database.
     * @param removeIfNull Determines if relation fields which are set to null should be processed or not.
     * @param removeFromList Determines if list relation fields should be processed for disjoining.
     * @param recursively Should children be updated recursively.
     * @param fields The relation fields which should be processed.
     */
    protected open suspend fun updateEntityWithChildren(entity: T, removeIfNull: Boolean = false, removeFromList: Boolean = true,
                                                  recursively: Boolean = false, vararg fields : String) {
        return updateEntityWithChildrenImpl(
            entity = entity,
            removeIfNull = removeIfNull,
            removeFromList = removeFromList,
            recursively = recursively,
            fields = fields)
    }

    private suspend fun updateEntityWithChildrenImpl(entity: T, removeIfNull: Boolean,
                                               removeFromList: Boolean,
                                               recursively: Boolean,
                                               vararg fields : String,
                                               processedEntities: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>> = mutableMapOf()) {
        if (entity.id <= 0)
            throw IllegalArgumentException("Entity has no id")

        // Do not process entities multiple times
        if (recursively && isInEntityCache(processedEntities, entity.id, entity::class))
            return

        // Update current entity
        this.asTypedIRoomInternalExDao().rupdate(entity)

        val fieldsToProcess =    fields.asList().filterNot { f -> f.isEmpty() }.emptyToNull()
                                 ?: relationFields.filter { r -> r.value.cascade.hasFlag(Cascade.Update) }.keys

        // loop over all given fields and update children
        for (field in fieldsToProcess)  {

            // skip invalid fields
            if (!relationFields.containsKey(field))
                throw IllegalArgumentException("Field $field is not a valid relation field for the current entity ${entity::class.simpleName}")
            if (!relationFields[field]!!.cascade.hasFlag(Cascade.Update))
                throw IllegalArgumentException("Field $field is not marked with 'updateCascade'")

            // Check whether to reverse id fields
            val isMappedField = relationFields[field]!!.mappedWith.isNotEmpty()

            // List with instanced entity values for this field
            // Must be nullable because we only process fields which are set
            var values: MutableList<RoomExEntity>? = null

            // Read instance values
            if (relationFields[field]?.type == List::class)
                values = this.asTypedIRoomInternalExDao().readPropertyList(entity, field) as MutableList<RoomExEntity>?
            else if (RoomExEntity::class.java.isAssignableFrom(relationFields[field]!!.childEntityClass.java)) {
                val value = this.asTypedIRoomInternalExDao().readPropertySingle(entity, field)
                if (value != null)
                    values = mutableListOf(value)
            }

            if (values == null && removeIfNull)
                values = mutableListOf()

            if (values == null)
                continue

            // Get current join for this relation
            val currentJoins = fetchJoins(entity.id, relationFields[field]!!.relationTableName, isMappedField)

            // Now loop over all current joins and look for disjoined entries
            if (relationFields[field]?.type != List::class || removeFromList) {
                var itIndex = 0
                while (itIndex < currentJoins.size) {
                    // Should entity not be found in the values list, it means that the relation has been disjoint
                    if (!values.any { x -> x.id == currentJoins[itIndex].childId }) {
                        disjoin(
                            entity.id,
                            currentJoins[itIndex].childId,
                            relationFields[field]!!.relationTableName,
                            isMappedField
                        )
                        currentJoins.removeAt(itIndex)
                    } else
                        itIndex++
                }
            }

            // Check if any new entity needs to be inserted first
            val childDao = fetchDaoForEntity(relationFields[field]!!.childEntityClass)

            for (it in values.filter { v -> v.id == 0L }) {
                if (!relationFields[field]!!.cascade.hasFlag(Cascade.Insert))
                    throw IllegalArgumentException("Unable to join field $field, since it has no id and is not marked for cascade insertion.")
                it.id = childDao.asTypedRoomExDao().asTypedIRoomInternalExDao().rinsert(it)
            }

            // Next step, insert the relation in the relation table
            for (it in values) {
                // If join is not in currentJoins, it means that it is a new join
                if (!currentJoins.any { x -> x.childId == it.id })
                    join(entity.id, it.id, relationFields[field]!!.relationTableName, isMappedField)
            }

            if (recursively) {
                // Now update the child and its children
                addToEntityCache(processedEntities, entity, entity::class)
                for (child in values) {
                    childDao.asTypedRoomExDao()
                        .updateEntityWithChildrenImpl(child, removeIfNull = removeIfNull, removeFromList = removeFromList, recursively = recursively)
                }
            }
        }
    }

    //endregion UPDATE

    //region DELETE

    /**
     * Deletes the given entity from the database.
     * Note: Relations will not be altered.
     *
     * @param entity The entity to remove.
     */
    @Delete
    protected abstract suspend fun deleteEntity(entity: T) : Int

    /**
     * Deletes the entity with the given id from the database.
     * Note: Relations will not be altered.
     *
     * @param id The id of the entity to remove.
     */
    protected open suspend fun deleteEntity(id: Long) {
        val tableName = this.asTypedIRoomInternalExDao().daoData.entityTableName

        @Suppress("unchecked_cast")
        this.asTypedIRoomExInternalDaoRawQueries()
            .deleteQuery(SimpleSQLiteQuery("DELETE FROM $tableName WHERE id = ?" , arrayOf(id)))
    }

    /**
     * Deletes the entity and its children with the "deleteCascade" flag from the database.
     * If no child field is given, all children with the "deleteCascade" flag will be deleted.
     *
     * @param entity The entity for which the relations should be updated.
     * @param fields The relation fields which should be processed.
     * @param recursively Should children be deleted recursively.
     *
     * @throws [IllegalArgumentException] If given relation field does not exist.
     * @throws [IllegalArgumentException] If entity id is not set.
     * @throws [IllegalArgumentException] If any given field is not marked with "deleteCascade = true".
     */
    protected open suspend fun deleteEntityWithChildren(entity: T, recursively: Boolean, vararg fields : String): Int {
        deleteEntityChildrenImpl(entity = entity,
            fields = fields,
            recursively = recursively)

        // Delete every ingoing join of this entity
        for (it in this.asTypedIRoomInternalExDao().daoData.relations.filter { !it.second }) {
            disjoinAll(entity.id, it.first, true)
        }

        return deleteEntity(entity)
    }

    /**
     * Deletes children with the "deleteCascade" flag from the database.
     * If no child field is given, all children with the "deleteCascade" flag will be deleted.
     *
     * @param entity The entity for which the relations should be updated.
     * @param recursively Should children be deleted recursively.
     * @param clearFromEntity If the relation fields should be cleared from the parent (children set to null).
     * @param fields The relation fields which should be processed.
     *
     * @throws [IllegalArgumentException] If given relation field does not exist.
     * @throws [IllegalArgumentException] If entity id is not set.
     * @throws [IllegalArgumentException] If any given field is not marked with "deleteCascade = true".
     */
    protected open suspend fun deleteEntityChildren(entity: T, recursively: Boolean, clearFromEntity: Boolean, vararg fields : String) {
        deleteEntityChildrenImpl(
            entity = entity,
            recursively = recursively,
            fields = fields)

        if (clearFromEntity) {
            for (field in fields) {
                if (relationFields[field]?.type == List::class) {
                    this.asTypedIRoomInternalExDao().writePropertyList(entity = entity, name = field, value = null)
                }
                else {
                    this.asTypedIRoomInternalExDao().writePropertySingle(entity = entity, name = field, value = null)
                }
            }
        }
    }

    private suspend fun deleteEntityChildrenImpl(entity: T,
                                                  vararg fields : String,
                                                  recursively: Boolean,
                                                  processedEntities: MutableMap<KClass<out RoomExEntity>, MutableMap<Long, RoomExEntity>> = mutableMapOf()) {
        if (entity.id <= 0)
            throw IllegalArgumentException("Entity has no id")

        // Do not process entities multiple times
        if (recursively && isInEntityCache(processedEntities, entity.id, entity::class))
            return

        val fieldsToProcess =   fields.asList().filterNot { f -> f.isEmpty() }.emptyToNull()
                                ?: relationFields.filter { r -> r.value.cascade.hasFlag(Cascade.Delete) }.keys

        // loop over all given fields and fetch the childdata
        for (field in fieldsToProcess)  {

            // skip invalid fields
            if (!relationFields.containsKey(field))
                throw IllegalArgumentException("Field $field is not a valid relation field for the current entity ${entity::class.simpleName}")
            if (!relationFields[field]!!.cascade.hasFlag(Cascade.Delete))
                throw IllegalArgumentException("Field $field is not marked with 'deleteCascade'")

            // Check whether to reverse id fields
            val isMappedField = relationFields[field]!!.mappedWith.isNotEmpty()

            // Get current join for this relation
            val currentJoins = fetchJoins(entity.id, relationFields[field]!!.relationTableName, isMappedField)
            val childDao = fetchDaoForEntity(relationFields[field]!!.childEntityClass).asTypedRoomExDao()
            val childIDao = fetchDaoForEntity(relationFields[field]!!.childEntityClass).asTypedRoomExDao().asTypedIRoomInternalExDao()

            // Now loop over all current joins and call deleteCascade recursively
            for(curJoin in currentJoins) {

                val curChild = childIDao.rread(curJoin.childId)
                if (curChild != null) {
                    if (recursively) {
                        addToEntityCache(processedEntities, curChild, curChild::class)
                        childDao.deleteEntityChildrenImpl(entity = curChild, recursively = true)
                    }
                    childIDao.rdelete(curChild)
                }
                // Delete every ingoing join of this child
                for (it in childIDao.daoData.relations.filter { !it.second }) {
                    disjoinAll(curJoin.childId, it.first, true)
                }

                // Delete relation from parent to current child
                disjoin(entity.id, curJoin.childId, relationFields[field]!!.relationTableName, isMappedField)
            }
        }
    }

    //endregion DELETE

    //region JOINS

    /**
     * Removes any existing join, if the corresponding field is null, and only null.
     * Only the join will be removed, not the corresponding child entity.
     *
     * @param entity The entity for which the relations should be updated.
     * @param fields Any field which needs to be checked for removal. When not passing any field name, all
     * relation fields are checked.
     */
    protected open suspend fun removeEntityJoinsIfNull(entity: T, vararg fields : String) {
        if (entity.id <= 0)
            throw IllegalArgumentException("Entity has no id")

        // loop over all given fields and remove any existing joins
        for (field in fields.asList().filterNot { f -> f.isEmpty() }.emptyToNull() ?: relationFields.keys)  {

            // skip invalid fields
            if (!relationFields.containsKey(field))
                throw IllegalArgumentException("Field $field is not a valid relation field for the current entity ${entity::class.simpleName}")

            // Check whether to reverse id fields
            val isMappedField = relationFields[field]!!.mappedWith.isNotEmpty()

            // List with instanced entity values for this field
            // Must be nullable because we only process fields which are set
            var values: MutableList<RoomExEntity>? = null

            // Read instance values
            if (relationFields[field]?.type == List::class)
                values = this.asTypedIRoomInternalExDao().readPropertyList(entity, field) as MutableList<RoomExEntity>?
            else if (RoomExEntity::class.java.isAssignableFrom(relationFields[field]!!.childEntityClass.java)) {
                val value = this.asTypedIRoomInternalExDao().readPropertySingle(entity, field)
                if (value != null)
                    values = mutableListOf(value)
            }

            // if field value is not null, skip this field
            if (values != null)
                continue

            disjoinAll(entity.id, relationFields[field]!!.relationTableName, isMappedField)
        }
    }

    /**
     * Joins the given entities with one another.
     * Should the child not have a valid id, it will be inserted in the database and
     * the new id will be set on the object
     *
     * @param entity Parent entity
     * @param child Child entity
     * @param fieldName The field name in the parent model
     */
    protected open suspend fun addEntityJoin(entity: T, child: RoomExEntity, fieldName: String) {
        if (!relationFields.containsKey(fieldName))
            throw IllegalArgumentException("Field $fieldName is not a valid relation field for the current entity ${entity::class.simpleName}")

        // Insert child if needed
        if (child.id == 0L) {
            if (!relationFields[fieldName]!!.cascade.hasFlag(Cascade.Read))
                throw IllegalArgumentException("Unable to join field $fieldName, since it has no id and is not marked for cascade insertion.")
            val dao = fetchDaoForEntity(relationFields[fieldName]!!.childEntityClass).asTypedIRoomInternalExDao()
            child.id = dao.rinsert(child)
        }

        // Check whether to reverse id fields
        val isMappedField = relationFields[fieldName]!!.mappedWith.isNotEmpty()

        join(entity.id, child.id, relationFields[fieldName]!!.relationTableName, isMappedField)
    }

    /**
     * Joins the given entities with one another.
     * Should any of the children not have a valid id, it will be inserted in the database and
     * the new id will be set on the object
     *
     * @param entity Parent entity
     * @param children Child entity
     * @param fieldName The field name in the parent model
     */
    protected open suspend fun addEntityJoins(entity: T, children: List<RoomExEntity>, fieldName: String) {
        if (!relationFields.containsKey(fieldName))
            throw IllegalArgumentException("Field $fieldName is not a valid relation field for the current entity ${entity::class.simpleName}")

        val dao = fetchDaoForEntity(relationFields[fieldName]!!.childEntityClass).asTypedIRoomInternalExDao()
        for (child in children) {

            // Insert child if needed
            if (child.id == 0L) {
                if (!relationFields[fieldName]!!.cascade.hasFlag(Cascade.Read))
                    throw IllegalArgumentException("Unable to join field $fieldName, since it has no id and is not marked for cascade insertion.")
                child.id = dao.rinsert(child)
            }

            // Check whether to reverse id fields
            val isMappedField = relationFields[fieldName]!!.mappedWith.isNotEmpty()

            join(entity.id, child.id, relationFields[fieldName]!!.relationTableName, isMappedField)
        }
    }

    /**
     * Removes the join between the given entities
     *
     * @param entity Parent entity
     * @param children Child entity
     * @param fieldName The field name in the parent model
     */
    protected open suspend fun removeEntityJoins(entity: T, children: List<RoomExEntity>, fieldName: String) {
        for (s in children) {
            removeEntityJoin(entity, s, fieldName)
        }
    }

    /**
     * Removes the join between the given entities
     *
     * @param entity Parent entity
     * @param child Child entity
     * @param fieldName The field name in the parent model
     */
    protected open suspend fun removeEntityJoin(entity: T, child: RoomExEntity, fieldName: String) {
        if (!relationFields.containsKey(fieldName))
            throw IllegalArgumentException("Field $fieldName is not a valid relation field for the current entity ${entity::class.simpleName}")
        if (child.id == 0L)
            throw IllegalArgumentException("Child does nat have a valid id")

        // Check whether to reverse id fields
        val isMappedField = relationFields[fieldName]!!.mappedWith.isNotEmpty()

        disjoin(entity.id, child.id, relationFields[fieldName]!!.relationTableName, isMappedField)
    }

    /**
     * Joins the given entity ids with one another.
     * Already existing relations will be ignored
     *
     * @param parentId Id of the parent entity.
     * @param childId Id of the child entity.
     * @param relTable The relation table created by RoomEx.
     * @param isMappedField If the relation annotation argument "mappedWith" is set.
     */
    private suspend fun join(parentId: Long, childId: Long, relTable: String, isMappedField: Boolean) {
        if (parentId > 0 && childId > 0) {
            val parentfieldname = if (!isMappedField) RoomExRelationEntity.PARENTFIELDNAME else RoomExRelationEntity.CHILDFIELDNAME
            val childfieldname = if (!isMappedField) RoomExRelationEntity.CHILDFIELDNAME else RoomExRelationEntity.PARENTFIELDNAME

            val query = "INSERT OR IGNORE INTO $relTable ($parentfieldname, $childfieldname) VALUES (?, ?)"
            this.asTypedIRoomExInternalDaoRawQueries().joinQuery(SimpleSQLiteQuery(query, arrayOf(parentId, childId)))
        }
    }

    /**
     * Removes an existing join between the given fields.
     *
     * @param parentId Id of the parent entity.
     * @param childId Id of the child entity.
     * @param relTable The relation table created by RoomEx.
     * @param isMappedField If the relation annotation argument "mappedWith" is set.
     */
    private suspend fun disjoin(parentId: Long, childId: Long, relTable: String, isMappedField: Boolean) {
        if (parentId > 0 && childId > 0) {
            val parentfieldname = if (!isMappedField) RoomExRelationEntity.PARENTFIELDNAME else RoomExRelationEntity.CHILDFIELDNAME
            val childfieldname = if (!isMappedField) RoomExRelationEntity.CHILDFIELDNAME else RoomExRelationEntity.PARENTFIELDNAME

            val query = "DELETE FROM $relTable WHERE $parentfieldname = ? AND $childfieldname = ?"
            this.asTypedIRoomExInternalDaoRawQueries().joinQuery(SimpleSQLiteQuery(query, arrayOf(parentId, childId)))
        }
    }

    /**
     * Removes all existing join between the given fields.
     *
     * @param id Id of the entity.
     * @param relTable The relation table created by RoomEx.
     * @param isMappedField If the relation annotation argument "mappedWith" is set.
     */
    private suspend fun disjoinAll(id: Long, relTable: String, isMappedField: Boolean) {
        if (id > 0) {
            val fieldName = if (!isMappedField) RoomExRelationEntity.PARENTFIELDNAME else RoomExRelationEntity.CHILDFIELDNAME

            val query = "DELETE FROM $relTable WHERE $fieldName = ?"
            this.asTypedIRoomExInternalDaoRawQueries().joinQuery(SimpleSQLiteQuery(query, arrayOf(id)))
        }
    }

    /**
     * Retrieves all existing joins between the given fields.
     *
     * @param parentId Id of the parent entity.
     * @param relTable The relation table created by RoomEx.
     * @param isMappedField If the relation annotation argument "mappedWith" is set.
     */
    private suspend fun fetchJoins(parentId: Long, relTable: String, isMappedField: Boolean): MutableList<RoomExRelationEntity> {
        val parentFieldName = if (!isMappedField) RoomExRelationEntity.PARENTFIELDNAME else RoomExRelationEntity.CHILDFIELDNAME
        val childFieldName = if (!isMappedField) RoomExRelationEntity.CHILDFIELDNAME else RoomExRelationEntity.PARENTFIELDNAME

        val query = "SELECT id, $parentFieldName, $childFieldName FROM $relTable WHERE $parentFieldName = $parentId"
        return this.asTypedIRoomExInternalDaoRawQueries().joinFetchQuery(SimpleSQLiteQuery(query, arrayOf()))
    }

    private suspend fun fetchAllChildren(parentId: Long, relTable: String, childTable: String, isMappedField: Boolean, ids: MutableList<Long>? = null): MutableList<T> {
        val parentFieldName = if (!isMappedField) RoomExRelationEntity.PARENTFIELDNAME else RoomExRelationEntity.CHILDFIELDNAME
        val childFieldName = if (!isMappedField) RoomExRelationEntity.CHILDFIELDNAME else RoomExRelationEntity.PARENTFIELDNAME

        val query: String
        if (ids == null)
            query = "SELECT * FROM $childTable WHERE id IN (SELECT $childFieldName FROM $relTable WHERE $parentFieldName = $parentId);"
        else
            query = "SELECT * FROM $childTable WHERE id IN (${ids.joinToString { it.toString() }});"

        return this.asTypedIRoomExInternalDaoRawQueries().fetchChildrenQuery(SimpleSQLiteQuery(query, arrayOf()))
    }

    //endregion JOINS

    //region HELPERS

    /**
     * Retrieves the DAO which corresponds to the provided entity class
     *
     * @param K Entity of the DAO
     * @param entityKClass Entity class of the DAO
     * @return Dao for the given entity. If no DAO exists, an error is thrown.
     */
    protected abstract fun <K : RoomExEntity> fetchDaoForEntity(entityKClass: KClass<K>): RoomExDao<K>

    /**
     * Converter method to cast IRoomExDao to IRoomExDao<RoomExEntity> suppressing the warning.
     * With the use of this method, the @Suppress annotation is also hidden from the user.
     *
     * @return The type explicit converted to IRoomExDao<RoomExEntity>
     */
    @Suppress("unchecked_cast")
    private fun asTypedIRoomInternalExDao() : IRoomExInternalDao<RoomExEntity> {
        return this as IRoomExInternalDao<RoomExEntity>
    }

    /**
     * Converter method to cast IRoomExDao to RoomExDao<RoomExEntity> suppressing the warning.
     * With the use of this method, the @Suppress annotation is also hidden from the user.
     *
     * @return The type explicit converted to RoomExDao<RoomExEntity>
     */
    @Suppress("unchecked_cast")
    private fun asTypedRoomExDao() : RoomExDao<RoomExEntity> {
        return this as RoomExDao<RoomExEntity>
    }

    @Suppress("unchecked_cast")
    private fun asTypedIRoomExInternalDaoRawQueries() : IRoomExInternalDaoRawQueries<T> {
        return this as IRoomExInternalDaoRawQueries<T>
    }

    //endregion HELPERS
}