package de.dutches.roomex.sample.storage.local.dao

import androidx.room.Query
import de.dutches.roomex.sample.model.User
import de.dutches.roomex.storage.RoomExDao
import de.dutches.roomex.storage.DaoEx
import de.dutches.roomex.storage.crud.ICrud

// Since this is only a sample class, suppress some warnings
@Suppress("unused")
@DaoEx
abstract class UserDao : RoomExDao<User>(), ICrud<User> {
    @Query("DELETE FROM rel_user_friends_user WHERE parentId = :userId AND childId = :friendId ")
    abstract suspend fun removeFriendJoin(userId: Long, friendId: Long)
}