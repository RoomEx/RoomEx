package de.dutches.roomex.processor.extensions

import kotlin.reflect.KClass

/**
 * Resolve the qualified, or the simple name of the given declaration.
 *
 * @param T Its type argument.
 * @return The name.
 */
internal fun <T : Any> KClass<T>.resolveName(simpleName: Boolean = false) : String {
    return  if (simpleName)
                this.simpleName.toString()
            else
                this.qualifiedName ?: this.simpleName.toString()
}