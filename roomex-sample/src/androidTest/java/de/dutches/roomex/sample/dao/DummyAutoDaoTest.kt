package de.dutches.roomex.sample.dao

import androidx.test.ext.junit.runners.AndroidJUnit4
import de.dutches.roomex.sample.model.Address
import de.dutches.roomex.sample.model.DummyAuto
import de.dutches.roomex.sample.model.User
import de.dutches.roomex.sample.storage.local.dao.AddressDao
import de.dutches.roomex.sample.storage.local.dao.UserDao
import de.dutches.roomex.storage.RoomExAutoDao
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class DummyAutoDaoTest : BaseDaoTest() {

    private lateinit var dummyAutoDao: RoomExAutoDao<DummyAuto>
    private lateinit var userDao: UserDao
    private lateinit var addressDao: AddressDao

    @Before
    fun setup() {
        dummyAutoDao = db.fetchDaoForEntity(DummyAuto::class) as RoomExAutoDao<DummyAuto>
        userDao = db.fetchDaoForEntity(User::class) as UserDao
        addressDao = db.fetchDaoForEntity(Address::class) as AddressDao
    }

    @After
    fun tearDown() {
        db.close()
    }



    //region CREATE & READ

    @Test
    fun insertNewDummyCrud() = runTest {
        // Given
        val dummyAuto = DummyAuto(sample = "ABC")

        // When
        val id = dummyAutoDao.insert(dummyAuto)

        // Then
        assertNotEquals(id, 0L)
    }

    @Test
    fun insertNewDummyCrudWithChildren() = runTest {
        // Given
        val dummyAuto = DummyAuto(sample = "ABC")
        dummyAuto.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        // When
        val id = dummyAutoDao.insertWithChildren(dummyAuto)

        // Then
        assertNotEquals(id, 0L)
    }

    @Test
    fun insertNewDummyCrudWithChildrenWithFields() = runTest {
        // Given
        val dummyAuto = DummyAuto(sample = "ABC")
        dummyAuto.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        // When
        val id = dummyAutoDao.insertWithChildren(dummyAuto, fields = arrayOf("users"))

        // Then
        assertNotEquals(id, 0L)
    }

    @Test
    fun readNewDummyCrudWithChildren() = runTest {
        // Given
        var dummyAuto: DummyAuto? = DummyAuto(sample = "ABC")
        dummyAuto!!.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        // When
        val id = dummyAutoDao.insertWithChildren(dummyAuto)
        dummyAuto = dummyAutoDao.getByIdWithChildren(id)

        // Then
        assertNotNull(dummyAuto)
        assertNotNull(dummyAuto!!.address)
        assertNotNull(dummyAuto.users)
        assertEquals(dummyAuto.users!!.count(), 2)
    }

    @Test
    fun readNewDummyCrudWithChildrenWithFields() = runTest {
        // Given
        var dummyAuto: DummyAuto? = DummyAuto(sample = "ABC")
        dummyAuto!!.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        // When
        val id = dummyAutoDao.insertWithChildren(dummyAuto)
        dummyAuto = dummyAutoDao.getByIdWithChildren(id, fields = arrayOf("users"))

        // Then
        assertNotNull(dummyAuto)
        assertNull(dummyAuto!!.address)
        assertNotNull(dummyAuto.users)
        assertEquals(dummyAuto.users!!.count(), 2)
    }

    //endregion CREATE & READ

    //region UPDATE

    @Test
    fun updateNewDummyCrudWithChildrenWithoutRemoveNull() = runTest {
        // Given
        var dummyAuto: DummyAuto? = DummyAuto(sample = "ABC")
        dummyAuto!!.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        dummyAuto.id = dummyAutoDao.insertWithChildren(dummyAuto)
        dummyAuto.sample = "XYZ"
        dummyAuto.address = null
        dummyAuto.users!!.removeLast()

        // When
        dummyAutoDao.updateWithChildren(dummyAuto)
        dummyAuto = dummyAutoDao.getByIdWithChildren(dummyAuto.id)

        // Then
        assertNotNull(dummyAuto)
        assertEquals("XYZ", dummyAuto!!.sample)
        assertNotNull(dummyAuto.address) // Should not be deleted
        assertNotNull(dummyAuto.users)
        assertEquals(dummyAuto.users!!.count(), 1)
    }

    @Test
    fun updateNewDummyCrudWithChildrenWithRemoveNull() = runTest {
        // Given
        var dummyAuto: DummyAuto? = DummyAuto(sample = "ABC")
        dummyAuto!!.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        dummyAuto.id = dummyAutoDao.insertWithChildren(dummyAuto)
        dummyAuto = dummyAutoDao.getByIdWithChildren(dummyAuto.id)!!
        dummyAuto.sample = "XYZ"
        dummyAuto.address = null
        dummyAuto.users!!.removeLast()

        // When
        dummyAutoDao.updateWithChildren(dummyAuto, true)
        dummyAuto = dummyAutoDao.getByIdWithChildren(dummyAuto.id)

        // Then
        assertNotNull(dummyAuto)
        assertEquals("XYZ", dummyAuto!!.sample)
        assertNull(dummyAuto.address) // Should be deleted
        assertNotNull(dummyAuto.users)
        assertEquals(1, dummyAuto.users!!.count())
    }

    //endregion UPDATE

    //region DELETE

    @Test
    fun deleteNewDummyCrudWithChildren() = runTest {
        // Given
        var dummyAuto: DummyAuto? = DummyAuto(sample = "ABC")
        dummyAuto!!.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        // When
        val id = dummyAutoDao.insertWithChildren(dummyAuto)
        dummyAuto = dummyAutoDao.getByIdWithChildren(id)!!
        dummyAutoDao.deleteWithChildren(dummyAuto)
        val dummyCrudDeleted = dummyAutoDao.getById(id)
        val addressDeleted = addressDao.getById(dummyAuto.address!!.id)
        val userDeleted = userDao.getById(dummyAuto.users!![0].id)

        // Then
        assertNotNull(dummyAuto)
        assertNotNull(dummyAuto.address)
        assertNotNull(dummyAuto.users)
        assertNull(dummyCrudDeleted)
        assertNull(addressDeleted)
        assertNull(userDeleted)
        assertEquals(dummyAuto.users!!.count(), 2)
    }

    @Test
    fun deleteNewDummyCrudChildren() = runTest {
        // Given
        var dummyAuto: DummyAuto? = DummyAuto(sample = "ABC")
        dummyAuto!!.address = Address()
        dummyAuto.users = mutableListOf(User(), User())

        // When
        val id = dummyAutoDao.insertWithChildren(dummyAuto)
        dummyAuto = dummyAutoDao.getByIdWithChildren(id)!!
        dummyAutoDao.deleteChildren(dummyAuto)
        val dummyCrudDeleted = dummyAutoDao.getById(id)!!
        val addressDeleted = addressDao.getById(dummyAuto.address!!.id)
        val userDeleted = userDao.getById(dummyAuto.users!![0].id)

        // Then
        assertNotNull(dummyAuto)
        assertNotNull(dummyAuto.address)
        assertNotNull(dummyAuto.users)
        assertNotNull(dummyCrudDeleted)
        assertNull(addressDeleted)
        assertNull(userDeleted)
        assertEquals(dummyAuto.users!!.count(), 2)
    }

    //endregion DELETE

}
