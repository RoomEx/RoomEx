package de.dutches.roomex.sample.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import de.dutches.roomex.storage.model.RoomExEntity
import de.dutches.roomex.storage.model.relation.Cascade
import de.dutches.roomex.storage.model.relation.ManyToManyEx
import de.dutches.roomex.storage.model.relation.OneToManyEx
import de.dutches.roomex.storage.model.relation.OneToOneEx

// Since this is only a sample class, suppress some warnings
@Suppress("unused")
@Entity(tableName = "user")
data class User(

    @ColumnInfo(name = "name")
    var name: String = "DEF",

    @ColumnInfo(name = "surname")
    var surname: String = "123"

) : RoomExEntity() {

    @Ignore
    @OneToOneEx(cascade = Cascade.All)
    var address : Address? = null

    @Ignore
    @OneToManyEx(cascade = Cascade.All)
    var children : List<User>? = null

    @Ignore
    @OneToOneEx(cascade = Cascade.InsertReadUpdate)
    var dad : User? = null

    @Ignore
    @ManyToManyEx(cascade = Cascade.All)
    var friends : MutableList<User>? = null

    @Ignore
    @ManyToManyEx(cascade = Cascade.All, entityKClass = Address::class)
    var otherAddresses : MutableList<IAddress>? = null

    override fun toString(): String {
        return """${System.lineSeparator()}
            User: $id --> $name, $surname
            address: $address
            children: ${children?.joinToString { c -> c.toString() }}
            dad: $dad
            friends: ${friends?.joinToString { f -> f.toString() }}
        """.trimIndent()
    }

}