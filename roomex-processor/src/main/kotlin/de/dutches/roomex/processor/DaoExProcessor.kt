package de.dutches.roomex.processor

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.google.devtools.ksp.closestClassDeclaration
import com.google.devtools.ksp.getAllSuperTypes
import com.google.devtools.ksp.isAbstract
import com.google.devtools.ksp.processing.CodeGenerator
import com.google.devtools.ksp.processing.Dependencies
import com.google.devtools.ksp.processing.KSPLogger
import com.google.devtools.ksp.symbol.*
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import de.dutches.roomex.processor.extensions.kotlinpoet.buildClassName
import de.dutches.roomex.processor.extensions.kotlinpoet.createConstructorCall
import de.dutches.roomex.processor.extensions.kotlinpoet.resolveTypeName
import de.dutches.roomex.processor.extensions.ksp.fetchAnnotationArgValue
import de.dutches.roomex.processor.extensions.ksp.fetchArgValue
import de.dutches.roomex.processor.extensions.ksp.resolveName
import de.dutches.roomex.processor.extensions.resolveName
import de.dutches.roomex.processor.helper.DaoUtils
import de.dutches.roomex.processor.helper.relations.RelationUtils.fetchEntityTypeForProperty
import de.dutches.roomex.processor.helper.relations.RelationUtils.fetchRelationTableName
import de.dutches.roomex.storage.DaoData
import de.dutches.roomex.storage.DaoEx
import de.dutches.roomex.storage.EntityRelPropData
import de.dutches.roomex.storage.IRoomExInternalDao
import de.dutches.roomex.storage.model.RoomExEntity
import kotlin.reflect.KClass

/**
 * The annotation processor which handles the [DaoEx] annotation.
 * This processor creates the needed
 *
 * @param codeGenerator
 * @param logger
 * @param options
 */
class DaoExProcessor(
    codeGenerator: CodeGenerator,
    logger: KSPLogger,
    options: Map<String, String>
) : AbstractProcessor(codeGenerator, logger, options) {

    override val annotationClass = DaoEx::class.java
    override fun getVisitor() = DaoExVisitor()

    val dbClassName = ClassName(Constants.roomExPackageStorage, Constants.roomExDatabaseClassName.removePrefix(Constants.roomExPackageStorage + "."))
    val roomExEntityTypeVar = TypeVariableName("T", RoomExEntity::class)

    protected inner class DaoExVisitor : AbstractProcessor.Visitor() {

        /** Entity class for this dao */
        private lateinit var entityClass: KSClassDeclaration
        private lateinit var entityClassTypeName : TypeName
        private lateinit var allSuperTypes : Sequence<KSType>

        /** List of constructor calls to initialize the relationFields property */
        private val relationFieldsList = mutableListOf<String>()

        /** List of relation properties */
        private val relationProperties = mutableMapOf<KSPropertyDeclaration, RelationProperty>()

        /**
         * Processes the given [DaoEx]-class and creates the corresponding [Dao]-class
         *
         * @param classDeclaration
         * @param data
         */
        override fun visitClassDeclaration(classDeclaration: KSClassDeclaration, data: Unit) {
            logger.info("Processing DAO: ${classDeclaration.simpleName.getShortName()}")

            // Fetch all super types of the current daoEx class
            allSuperTypes = classDeclaration.getAllSuperTypes()

            // Super handles the creation of the FileBuilder
            super.visitClassDeclaration(classDeclaration, data)

            // Build the new dao class and append "Ex" to the original name
            val builderClass = TypeSpec.classBuilder(classDeclaration.simpleName.getShortName() + "Ex")

            // Add the Room @Dao
            builderClass.addAnnotation(AnnotationSpec.builder(Dao::class).build())

            // Make the class abstract
            builderClass.addModifiers(KModifier.ABSTRACT)

            // Import and set super class/interface
            builderFile.addImport(classDeclaration.packageName.asString(), classDeclaration.simpleName.asString())

            // Retrieve the entity class for this dao
            val entityClassType = DaoUtils.fetchEntityForDao(classDeclaration)
            entityClassTypeName = entityClassType.resolveTypeName()
            entityClass = entityClassType.declaration.closestClassDeclaration()!!

            // Add the current class as superclass
            builderClass.superclass(classDeclaration.buildClassName())
            // Add IRoomExDao interface
            builderClass.addSuperinterface(IRoomExInternalDao::class.buildClassName().parameterizedBy(entityClassTypeName))
            // Add RoomExDatabase as constructor argument, so that Room passes the database implementation
            builderClass.primaryConstructor(FunSpec.constructorBuilder().addParameter("db", dbClassName).build())
            // Also add as property. so that KotlinPoet creates the constructor argument with val/var
            builderClass.addProperty(PropertySpec.builder("db", dbClassName).initializer("db").build())

            // We're done with the class declaration, visit the properties
            entityClass.getAllProperties().forEach { p -> visitEntityPropertyDeclaration(p) }

            // Implement any method inherited by any RoomEx interface or class
            val funSpecs = implementMethods()

            // Implement any property inherited by any RoomEx interface or class
            val propSpecs = implementProperties()

            // Everything created, build the final class
            builderClass.addFunctions(funSpecs)
            builderClass.addProperties(propSpecs)

            // Build the final class
            builderFile.addType(builderClass.build())

            var daoFileContent = builderFile.build().toString()

            // Add the IRoomExInternalDaoRawQueries class as super class
            // Package with the IRoomExInternalDaoRawQueries class cannot be added as a dependency, since it is an android package
            // Therefore we need to add it manually, post class generation
            daoFileContent = StringBuilder(daoFileContent)
                .insert(daoFileContent.indexOf(" ${Constants.roomExInternalDaoInterfaceName.removePrefix(Constants.roomExPackageStorage + ".") }<"),
                    " ${Constants.roomExInternalDaoRawQueriesInterfaceName}<${entityClassType.declaration.simpleName.getShortName()}>, ").toString()

            // Write the new dao class to a file
            addNewCodeFile(classDeclaration.packageName.asString(),
                classDeclaration.simpleName.asString() + classPostfix,
                daoFileContent,
                if (classDeclaration.containingFile != null) Dependencies(false, classDeclaration.containingFile!!)
                else Dependencies(false, *resolver.getAllFiles().toList().toTypedArray())
            )
        }

        override fun implementMethods() : List<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            var tableName = entityClass.fetchAnnotationArgValue(Entity::class, "tableName") as String
            if (tableName.isBlank())
                tableName = entityClass.simpleName.asString()

            // Read method, implementation from interface
            funSpecs += FunSpec
                .builder("rread")
                .addModifiers(KModifier.ABSTRACT, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("id", Long::class)
                .returns(entityClassTypeName.copy(true))
                .addAnnotation(AnnotationSpec
                    .builder(Query::class)
                    .addMember("\"SELECT * FROM $tableName WHERE id = :id\"")
                    .build()
                ).build()

            // Read all method, implementation from interface
            funSpecs += FunSpec
                .builder("rreadAll")
                .addModifiers(KModifier.ABSTRACT, KModifier.OVERRIDE, KModifier.SUSPEND)
                .returns(ClassName("kotlin.collections", "MutableList").parameterizedBy(entityClassTypeName))
                .addAnnotation(AnnotationSpec
                    .builder(Query::class)
                    .addMember("\"SELECT * FROM $tableName\"")
                    .build()
                ).build()

            // Read method, implementation from interface
            funSpecs += FunSpec
                .builder("rexists")
                .addModifiers(KModifier.ABSTRACT, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("id", Long::class)
                .returns(Boolean::class)
                .addAnnotation(AnnotationSpec
                    .builder(Query::class)
                    .addMember("\"SELECT EXISTS(SELECT * FROM $tableName WHERE id = :id)\"")
                    .build()
                ).build()

            // Method to fetch dao based on entity type, implementation from base class
            funSpecs += FunSpec
                .builder("fetchDaoForEntity")
                .addTypeVariable(roomExEntityTypeVar)
                .addModifiers(KModifier.OVERRIDE, KModifier.PUBLIC)
                .returns(ClassName.bestGuess(Constants.roomExDaoClassName).parameterizedBy(roomExEntityTypeVar))
                .addParameter(ParameterSpec("entityKClass",
                    KClass::class.buildClassName().parameterizedBy(roomExEntityTypeVar)))
                .addCode("return db.fetchDaoForEntity(entityKClass)")
                .build()

            funSpecs += implementReadProperties()
            funSpecs += implementWriteProperties()

            if (allSuperTypes.any { t -> t.declaration.resolveName() == Constants.roomExCrudCreateInterfaceName })
                funSpecs += implementCrudCreate()

            if (allSuperTypes.any { t -> t.declaration.resolveName() == Constants.roomExCrudReadInterfaceName })
                funSpecs += implementCrudRead()

            if (allSuperTypes.any { t -> t.declaration.resolveName() == Constants.roomExCrudUpdateInterfaceName })
                funSpecs += implementCrudUpdate()

            if (allSuperTypes.any { t -> t.declaration.resolveName() == Constants.roomExCrudDeleteInterfaceName })
                funSpecs += implementCrudDelete()

            if (allSuperTypes.any { t -> t.declaration.resolveName() == Constants.roomExCrudJoinsInterfaceName })
                funSpecs += implementCrudJoins()

            return funSpecs
        }

        private fun implementCrudCreate() : MutableList<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            funSpecs += FunSpec
                .builder("insert")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND, KModifier.ABSTRACT)
                .addAnnotation(Insert::class.buildClassName())
                .addParameter("entity", entityClassTypeName)
                .returns(Long::class)
                .build()

            funSpecs += FunSpec
                .builder("insertWithChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("recursively", Boolean::class)
                .returns(Long::class)
                .addCode("return super.insertEntityWithChildren(" +     System.lineSeparator() +
                         "    entity = entity," +                       System.lineSeparator() +
                         "    recursively = recursively)")
                .build()

            funSpecs += FunSpec
                .builder("insertWithChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("recursively", Boolean::class)
                .addParameter("fields", String::class, KModifier.VARARG)
                .returns(Long::class)
                .addCode("return super.insertEntityWithChildren(" +     System.lineSeparator() +
                         "    entity = entity," +                       System.lineSeparator() +
                         "    recursively = recursively," +             System.lineSeparator() +
                         "    fields = fields)")
                .build()

            return funSpecs
        }

        private fun implementCrudRead() : MutableList<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            funSpecs += FunSpec
                .builder("exists")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("id", Long::class)
                .returns(Boolean::class)
                .addCode("return super.existsEntity(" +                 System.lineSeparator() +
                         "    id = id)")
                .build()

            funSpecs += FunSpec
                .builder("getById")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("id", Long::class)
                .returns(entityClassTypeName.copy(true))
                .addCode("return super.getEntityById(" +                System.lineSeparator() +
                         "    id = id)")
                .build()

            funSpecs += FunSpec
                .builder("getByIdWithChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("id", Long::class)
                .addParameter("recursively", Boolean::class)
                .addParameter("fields", String::class, KModifier.VARARG)
                .returns(entityClassTypeName.copy(true))
                .addCode("return super.getEntityByIdWithChildren(" +    System.lineSeparator() +
                         "    id = id, " +                              System.lineSeparator() +
                         "    recursively = recursively," +             System.lineSeparator() +
                         "    fields = fields)")
                .build()

            funSpecs += FunSpec
                .builder("getAll")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .returns(ClassName("kotlin.collections", "MutableList")
                    .parameterizedBy(entityClassTypeName))
                .addCode("val ls = super.getAllEntities()\n" +
                        "return ls")
                .build()

            funSpecs += FunSpec
                .builder("loadChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("recursively", Boolean::class)
                .returns(entityClassTypeName)
                .addCode("return super.loadEntityChildren(" +           System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    recursively = recursively)")
                .build()

            funSpecs += FunSpec
                .builder("loadChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("recursively", Boolean::class)
                .addParameter("fields", String::class, KModifier.VARARG)
                .returns(entityClassTypeName)
                .addCode("return super.loadEntityChildren(" +           System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    recursively = recursively," +              System.lineSeparator() +
                        "    fields = fields)")
                .build()

            return funSpecs
        }

        private fun implementCrudUpdate() : MutableList<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            funSpecs += FunSpec
                .builder("update")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND, KModifier.ABSTRACT)
                .addAnnotation(Update::class.buildClassName())
                .addParameter("entity", entityClassTypeName)
                .build()

            funSpecs += FunSpec
                .builder("updateWithChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("removeIfNull", Boolean::class)
                .addParameter("removeFromList", Boolean::class)
                .addParameter("recursively", Boolean::class)
                .addCode("return super.updateEntityWithChildren(" +     System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    removeIfNull = removeIfNull, " +           System.lineSeparator() +
                        "    removeFromList = removeFromList," +        System.lineSeparator() +
                        "    recursively = recursively)")
                .build()

            funSpecs += FunSpec
                .builder("updateWithChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("removeIfNull", Boolean::class)
                .addParameter("removeFromList", Boolean::class)
                .addParameter("recursively", Boolean::class)
                .addParameter("fields", String::class, KModifier.VARARG)
                .addCode("return super.updateEntityWithChildren(" +     System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    removeIfNull = removeIfNull, " +           System.lineSeparator() +
                        "    removeFromList = removeFromList, " +       System.lineSeparator() +
                        "    recursively = recursively," +              System.lineSeparator() +
                        "    fields = fields)")
                .build()

            return funSpecs
        }

        private fun implementCrudDelete() : MutableList<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            funSpecs += FunSpec
                .builder("delete")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND, KModifier.ABSTRACT)
                .addAnnotation(Delete::class.buildClassName())
                .addParameter("entity", entityClassTypeName)
                .returns(Int::class)
                .build()

            funSpecs += FunSpec
                .builder("delete")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("id", Long::class)
                .addCode("return super.deleteEntity(" +                 System.lineSeparator() +
                         "    id = id)")
                .build()

            funSpecs += FunSpec
                .builder("deleteWithChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("recursively", Boolean::class)
                .addParameter("fields", String::class, KModifier.VARARG)
                .returns(Int::class)
                .addCode("return super.deleteEntityWithChildren(" +     System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    recursively = recursively, " +             System.lineSeparator() +
                        "    fields = fields)")
                .build()

            funSpecs += FunSpec
                .builder("deleteChildren")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("recursively", Boolean::class)
                .addParameter("clearFromEntity", Boolean::class)
                .addParameter("fields", String::class, KModifier.VARARG)
                .addCode("return super.deleteEntityChildren(" +         System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    recursively = recursively, " +             System.lineSeparator() +
                        "    clearFromEntity = clearFromEntity," +      System.lineSeparator() +
                        "    fields = fields)")
                .build()

            return funSpecs
        }

        private fun implementCrudJoins() : MutableList<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            funSpecs += FunSpec
                .builder("addJoin")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("child", RoomExEntity::class.buildClassName())
                .addParameter("fieldName", String::class)
                .addCode("return super.addEntityJoin(" +                System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    child = child, " +                         System.lineSeparator() +
                        "    fieldName = fieldName)")
                .build()

            funSpecs += FunSpec
                .builder("addJoins")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("children", List::class.buildClassName().parameterizedBy(RoomExEntity::class.buildClassName()))
                .addParameter("fieldName", String::class)
                .addCode("return super.addEntityJoins(" +               System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    children = children, " +                   System.lineSeparator() +
                        "    fieldName = fieldName)")
                .build()

            funSpecs += FunSpec
                .builder("removeJoin")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("child", RoomExEntity::class.buildClassName())
                .addParameter("fieldName", String::class)
                .addCode("return super.removeEntityJoin(" +             System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    child = child, " +                         System.lineSeparator() +
                        "    fieldName = fieldName)")
                .build()

            funSpecs += FunSpec
                .builder("removeJoins")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("children", List::class.buildClassName().parameterizedBy(RoomExEntity::class.buildClassName()))
                .addParameter("fieldName", String::class)
                .addCode("return super.removeEntityJoins(" +            System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    children = children, " +                   System.lineSeparator() +
                        "    fieldName = fieldName)")
                .build()

            funSpecs += FunSpec
                .builder("removeJoinsIfNull")
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE, KModifier.SUSPEND)
                .addParameter("entity", entityClassTypeName)
                .addParameter("fields", String::class, KModifier.VARARG)
                .addCode("return super.removeEntityJoinsIfNull(" +      System.lineSeparator() +
                        "    entity = entity, " +                       System.lineSeparator() +
                        "    fields = fields)")
                .build()

            return funSpecs
        }

        private fun implementReadProperties() : List<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            val builderReadSingle = FunSpec
                .builder("readPropertySingle")
                .addModifiers(KModifier.OVERRIDE, KModifier.PUBLIC)
                .returns(RoomExEntity::class.buildClassName().copy(true))
                .addParameter("entity", entityClassTypeName)
                .addParameter("name", String::class)

            if (relationProperties.filter { !it.value.isCollection }.any()) {
                builderReadSingle.beginControlFlow("return when(name)")
                relationProperties.forEach { (p, typeDef) ->
                    if (!typeDef.isCollection) {
                        // Should it be an interface, we need to cast to RoomExEntity?
                        if (typeDef.needsCast())
                            builderReadSingle.addStatement("\"${p.simpleName.asString()}\" -> entity.${p.simpleName.asString()} as ${typeDef.entityType.declaration.resolveName()}?")
                        else
                            builderReadSingle.addStatement("\"${p.simpleName.asString()}\" -> entity.${p.simpleName.asString()}")
                    }
                }
                builderReadSingle
                    .addStatement("else -> throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
                    .endControlFlow()
            }
            else {
                builderReadSingle.addStatement("throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
            }
            funSpecs += builderReadSingle.build()



            val builderReadList = FunSpec
                .builder("readPropertyList")
                .addAnnotation(AnnotationSpec.builder(Suppress::class).addMember("names = [\"unchecked_cast\"]").build())
                .addModifiers(KModifier.OVERRIDE, KModifier.PUBLIC)
                .returns(List::class.buildClassName().parameterizedBy(RoomExEntity::class.buildClassName()).copy(true))
                .addParameter("entity", entityClassTypeName)
                .addParameter("name", String::class)

            if (relationProperties.filter { it.value.isCollection }.any()) {
                builderReadList.beginControlFlow("return when(name)")
                relationProperties.filter { it.value.isCollection }.forEach { (p, typeDef) ->

                    // Should it be an interface, we need to cast to RoomExEntity?
                    if (typeDef.needsCast()) {
                        builderReadList.addStatement("\"${p.simpleName.asString()}\" -> entity.${p.simpleName.asString()} as MutableList<RoomExEntity>?")
                    }
                    else {
                        builderReadList.addStatement("\"${p.simpleName.asString()}\" -> entity.${p.simpleName.asString()}")
                    }
                }
                builderReadList
                    .addStatement("else -> throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
                    .endControlFlow()
            }
            else {
                builderReadList.addStatement("throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
            }

            funSpecs += builderReadList.build()

            return funSpecs
        }


        private fun implementWriteProperties() : List<FunSpec> {
            val funSpecs = mutableListOf<FunSpec>()

            val builderWriteSingle = FunSpec
                .builder("writePropertySingle")
                .addModifiers(KModifier.OVERRIDE, KModifier.PUBLIC)
                .addParameter("entity", entityClassTypeName)
                .addParameter("name", String::class)
                .addParameter("value", RoomExEntity::class.buildClassName().copy(true))

            if (relationProperties.filter { !it.value.isCollection }.any()) {
                builderWriteSingle.beginControlFlow("when(name)")
                relationProperties.forEach { (p, typeDef) ->
                    if (!typeDef.isCollection) {
                        builderWriteSingle.addStatement("\"${p.simpleName.asString()}\" -> entity.${p.simpleName.asString()} = value as ${typeDef.entityType.declaration.resolveName()}?")
                    }
                }
                builderWriteSingle
                    .addStatement("else -> throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
                    .endControlFlow()
            }
            else {
                builderWriteSingle
                    .addStatement("throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
            }
            funSpecs += builderWriteSingle
                .returns(Unit::class)
                .build()


            val builderWriteList = FunSpec
                .builder("writePropertyList")
                .addAnnotation(AnnotationSpec.builder(Suppress::class).addMember("names = [\"unchecked_cast\"]").build())
                .addModifiers(KModifier.OVERRIDE, KModifier.PUBLIC)
                .addParameter("entity", entityClassTypeName)
                .addParameter("name", String::class)
                .addParameter("value", ClassName("kotlin.collections", "MutableList").parameterizedBy(RoomExEntity::class.buildClassName()).copy(true))

            if (relationProperties.filter { it.value.isCollection }.any()) {
                builderWriteList.beginControlFlow("when(name)")
                relationProperties.forEach { (p, typeDef) ->
                    if (typeDef.isCollection)
                        builderWriteList.addStatement("\"${p.simpleName.asString()}\" -> entity.${p.simpleName.asString()} = value as MutableList<${typeDef.propertyInnerType!!.declaration.resolveName()}>?")
                }

                builderWriteList
                    .addStatement("else -> throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
                    .endControlFlow()
            }
            else {
                builderWriteList
                    .addStatement("throw IllegalArgumentException(\"An invalid property name has been passed: \" + name)")
            }
            funSpecs += builderWriteList
                .returns(Unit::class)
                .build()

            return funSpecs
        }

        override fun implementProperties() : List<PropertySpec> {
            val propSpecs = mutableListOf<PropertySpec>()

            // Property with all infos about outgoing relations
            propSpecs += PropertySpec
                .builder("relationFields", Map::class.buildClassName()
                    .parameterizedBy(String::class.buildClassName(), EntityRelPropData::class.buildClassName()))
                .initializer("mapOf(${relationFieldsList.joinToString { s -> s } })")
                .addModifiers(KModifier.OVERRIDE, KModifier.PROTECTED)
                .build()

            // Property with all other infos, incl. ingoing relations
            val relations = CollectorProcessor
                .allEntitiesWithRelations[entityClass.asStarProjectedType()]!!
                .map { "\"${it.key }\" to ${it.value}" }.joinToString { it }

            propSpecs += PropertySpec
                .builder("daoData", DaoData::class.buildClassName())
                .initializer("DaoData(entityClass = ${entityClass.simpleName.getShortName()}::class, " +    System.lineSeparator() +
                             "    entityTableName = \"${fetchEntityTableName(entityClass)}\"," +            System.lineSeparator() +
                             "    relations = listOf($relations))")
                .addModifiers(KModifier.OVERRIDE, KModifier.PUBLIC)
                .build()

            return propSpecs
        }

        private fun visitEntityPropertyDeclaration(property: KSPropertyDeclaration) {

            // Only need to process this property if it has a relationship annotation
            val annotation = getRelationshipAnnotation(property)
            if (annotation == null)
                return

            val (_, childEntityType) = fetchEntityTypeForProperty(property,
                CollectorProcessor.allEntitiesWithRelations.keys,
                CollectorProcessor.entityInterfaceMapping)
            if (childEntityType == null) {
                logger.error("Unable to resolve property ${property.simpleName.asString()} to an entity.", property)
                return
            }

            // Get value of "mappedWith"
            // Note: Should mappedWith not be empty, the relation table of the mapped field is used and the usage
            // of the parent- and childids are reverse
            val mappedWith = annotation.fetchArgValue(Constants.Annotations.Relations.Arguments.MAPPED_WITH) as String

            // Get value of cascade
            val cascade = annotation.fetchArgValue(Constants.Annotations.Relations.Arguments.CASCADE).toString()

            val relTableName = fetchRelationTableName(entityClass, childEntityType.declaration as KSClassDeclaration, property, mappedWith)

            // Add the type as import
            builderFile.addImport(childEntityType.declaration.packageName.asString(), childEntityType.declaration.simpleName.asString())

            // Get raw property type, as it might be a collection
            val rawPropType = property.type.resolve()

            // Add the type to the property list
            val innerType = if (rawPropType.arguments.any()) rawPropType.arguments.first().type?.resolve() else null
            relationProperties[property] = RelationProperty(childEntityType, rawPropType, innerType)

            // Add the type to the relation list on the dao
            relationFieldsList += "\"${property.simpleName.asString()}\" to ${
                    EntityRelPropData::class.buildClassName().createConstructorCall(

                    // First argument - Type of field, can be generic
                    "${rawPropType.declaration.qualifiedName?.asString()}::class",

                    // Second argument - Entity type of field
                    "${childEntityType}::class".replace("?",""),

                    // Third argument - Relation table name
                    "\"$relTableName\"",

                    // Forth argument - mappedWith
                    "\"$mappedWith\"",

                    // Fifth argument - cascade 
                    cascade,
                        
                    // Sixth argument - Child entity table name
                    "\"${fetchEntityTableName(childEntityType.declaration as KSClassDeclaration)}\""
                )
            }"
        }

        private fun fetchEntityTableName(entityClass: KSClassDeclaration) : String {
            val tableName = entityClass.fetchAnnotationArgValue(Entity::class, "tableName") as String
            return  if (tableName.isBlank()) entityClass.resolveName(true) else tableName.trim()
        }

         /**
         * Checks if the given class declaration is valid.
         * A valid class is abstract, implements RoomExDao and is not annotated with @Dao.
         * Should the class be invalid, a message is added to the log.
         *
         * @param declaration The class to validate
         * @return True if valid, false if not
         */
        override fun checkAndLogClassValidation(declaration : KSClassDeclaration) : Boolean {
             var isValid = true

             // Check if set on an abstract class
             if (declaration.classKind != ClassKind.CLASS || !declaration.isAbstract())
             {
                 logger.error("Annotation \"${DaoEx::class.simpleName}\" can only be used on abstract classes.", declaration)
                 isValid = false
             }

             // Check if RoomExDao exists as super class
             if (allSuperTypes.none { s -> s.declaration.buildClassName().toString() == Constants.roomExDaoClassName })
             {
                 logger.error("Classes annotated with the \"${DaoEx::class.simpleName}\" annotation must have the \"${Constants.roomExDaoClassName}\" supertype.", declaration)
                 isValid = false
             }

             // Check if also annotated with @Dao
             if (allSuperTypes.flatMap { s -> s.annotations }.any { a -> a.resolveName() == Dao::class.resolveName() })
             {
                 logger.error("Classes annotated with the \"${DaoEx::class.simpleName}\" can not also be annotated with \"${Dao::class.simpleName}\".", declaration)
                 isValid = false
             }
             return isValid
        }

        private fun getRelationshipAnnotation(property: KSPropertyDeclaration) : KSAnnotation?
            = property.annotations.firstOrNull { a -> a.resolveName() in Constants.Annotations.Relations.relationshipAnnotations.values }


        private inner class RelationProperty(
            val entityType: KSType,
            val propertyType: KSType,
            val propertyInnerType: KSType?) {
            val isCollection: Boolean = propertyInnerType != null
            fun needsCast(): Boolean {
                return if (isCollection)
                    entityType.declaration.resolveName() != propertyInnerType!!.declaration.resolveName()
                else
                    entityType.declaration.resolveName() != propertyType.declaration.resolveName()
            }



        }
    }
}