package de.dutches.roomex.sample.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import de.dutches.roomex.storage.AutoDao
import de.dutches.roomex.storage.model.RoomExEntity
import de.dutches.roomex.storage.model.relation.Cascade
import de.dutches.roomex.storage.model.relation.OneToManyEx
import de.dutches.roomex.storage.model.relation.OneToOneEx

// Since this is only a sample class, suppress some warnings
@Suppress("unused")
@Entity
@AutoDao
data class DummyAuto(

    @ColumnInfo
    var sample: String

) : RoomExEntity() {

    @Ignore @OneToManyEx(cascade = Cascade.All)
    var users: MutableList<User>? = null

    @Ignore @OneToOneEx(cascade = Cascade.All)
    var address: Address? = null
}