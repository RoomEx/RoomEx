package de.dutches.roomex.sample.view.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import de.dutches.roomex.sample.model.User
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
) : ViewModel() {

    lateinit var users: LiveData<List<User>>
}