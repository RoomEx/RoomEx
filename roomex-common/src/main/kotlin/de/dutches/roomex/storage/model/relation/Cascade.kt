@file:Suppress("unused")
package de.dutches.roomex.storage.model.relation

enum class Cascade(val value: Int) {
    Insert(1),
    Read(2),
    Update(4),
    Delete(8),
    InsertRead(3),
    InsertUpdate(5),
    InsertDelete(9),
    InsertReadUpdate(7),
    InsertReadDelete(11),
    InsertUpdateDelete(13),
    ReadUpdate(6),
    ReadDelete(10),
    ReadUpdateDelete(14),
    UpdateDelete(12),
    All(15),
    None(0);

    fun hasFlag(cascade: Cascade) : Boolean {
        return ((this.value and cascade.value) == cascade.value)
    }
}