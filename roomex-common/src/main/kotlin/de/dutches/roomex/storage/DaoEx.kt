@file:Suppress("unused")
package de.dutches.roomex.storage

/**
 * Annotation to use for DAO-classed.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class DaoEx